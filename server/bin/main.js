#!/bin/env node
let resolve = require("path").resolve
let root = (...args) => resolve(__dirname, "../..", ...args)
require(root('.pnp.cjs')).setup()
require('ts-node').register({
  transpileOnly: true
})
let spawn = require('pinospace').spawn



let argvtype = require("argvtype")
let h = require("hyperval")

const cmd = new argvtype.Command()
  .name("server")
  .description(`Spawn a server used to manage and issue nebula certificates.

The command will print out any keys and IPs issued.`)
  .example("Regular usage", "/usr/bin/nebula-cert /etc/nebula/autocerts 192.168.0.0/24 some_long_and_secure_secret")
  .flag("keyrotate", flag => flag
    .description("Interval in milliseconds to rotate security keys.")
    .type("milliseconds", h.cast(x => +x)(h.number(x => x > 0 && Number.isInteger(x))).apply)
    .default(5000)
  )
  .flag("ca_name", flag => flag
    .description("Name of certificate authority.")
    .letter("n")
    .type("string", h.string().apply)
    .default("Nebula-DHCP")
  )
  .flag("ca_duration", flag => flag
    .description("How long the CA will be valid for.")
    .letter("d")
    .type("milliseconds", h.cast(x => +x)(h.number(x => x > 0 && Number.isInteger(x))).apply)
    .default(1000 * 60 * 60 * 24 * 365 * 10)
  )
  .flag("cert_lease", flag => flag
    .description("How long an issued certificate will be valid for.")
    .letter("l")
    .type("milliseconds", h.cast(x => +x)(h.number(x => x > 0 && Number.isInteger(x))).apply)
    .default(1000 * 60 * 60 * 24)
  )
  .flag("database", flag => flag
    .description("Database location where the server stores information of all certs issued and available IPs.")
    .type("path", h.string().apply)
    .default("./.db")
  )
  .flag("ip", flag => flag
    .description("Address for server to bind to.")
    .letter("i")
    .type("ip", h.string().apply)
    .default("0.0.0.0")
  )
  .flag("port", flag => flag
    .description("Port for server bind to.")
    .letter('p')
    .type("1-65535", h.cast(x => +x)(h.number(x => x > 0 && x < 0xFFFF && Number.isInteger(x))).apply)
    .default(36742)
  )
  .flag("static_ip", flag => flag
    .description("Static IPs to not issue during dynamic get. Written in CIDR notation.")
    .letter("s")
    .type("cidr", x => x)
    .variadic()
    .optional()
  )
  .arguments(args => args
    .arg("binary", a => a
      .description("The 'nebula-cert' binary location.")
      .type("path", x => x)
      .required()
    )
    .arg("folder", a => a
      .description("The folder to store certificates in.")
      .type("path", x => x)
      .required()
    )
    .arg("pool", a => a
      .description("A pool of ip addresses to use. Written in CIDR notation.")
      .type("cidr", x => x)
      .required()
    )
    .arg("secret", a => a
      .description("A shared secret between the server and the client.")
      .type("string", x => x)
      .required()
    )
  ).action(async ({ args: { binary, folder, pool, secret }, flags: { ip, ca_duration, ca_name, cert_lease, database, keyrotate, port, static_ip } }) => {

    await require("../src/controller").controller(
      folder,
      binary,
      keyrotate,
      ca_name,
      ca_duration,
      cert_lease,
      require("@nebula-dhcp/ip").IP.from_cidr(pool),
      static_ip.map(require("@nebula-dhcp/ip").IP.from_cidr),
      { port, ip },
      database,
      secret,
      spawn("server")
    )
  })

argvtype.cli(process.argv.slice(2), cmd)
