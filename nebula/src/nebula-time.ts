/**
 * Converts milliseconds into hms format `1h0m30s`
 * 
 * @param time Milliseconds to convert
 * @returns `string` in hms format `1h0m30s`
 */
export function nebula_time(time: number) {
  const milliseconds = time % 1000
  time = (time - milliseconds) / 1000
  const seconds = time % 60
  time = (time - seconds) / 60
  const minutes = time % 60
  time = (time - minutes) / 60

  return `${time}h${minutes}m${seconds}s`
}