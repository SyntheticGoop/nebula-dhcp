import type { HttpEffectResponse } from '@marblejs/core';
import { HttpError, HttpStatus, r } from '@marblejs/core';
import { number, string, struct } from 'hyperval';
import type { Observable } from 'rxjs';
import { catchError, map, mergeMap, throwError } from 'rxjs';

const sign_dynamic_schema = struct({
  key_pub: string()
})


export const sign_dynamic_cert = (
  decrypt: (payload: string) => Observable<{ payload: unknown, nonce: string }>,
  encrypt: (nonce: string, payload: any) => string,
  sign: (key_pub: string) => Observable<{ ca_crt: string, key_crt: string, ip: string }>,
) => r.pipe(
  r.matchPath('/dynamic'),
  r.matchType('POST'),
  r.useEffect(req => req.pipe(
    mergeMap(req => {
      if (typeof req.body !== 'string') {
        console.log("Dynamic IP request rejected on failed authentication.")
        throw new HttpError('No authentication provided', HttpStatus.UNAUTHORIZED)
      }
      return decrypt(req.body)
    }),
    catchError(e => throwError(() => e instanceof HttpError ? e : new HttpError('Authentication failed', HttpStatus.FORBIDDEN))),
    map(struct({ payload: sign_dynamic_schema, nonce: string() }).apply),
    catchError(e => throwError(() => e instanceof HttpError ? e : new HttpError('Invalid schema', HttpStatus.BAD_REQUEST))),
    mergeMap(({ payload, nonce }) =>
      sign(payload.key_pub).pipe(
        map(certs => (
          console.log(`Dynamic IP issued ${certs.ip}`),
          encrypt(nonce, certs)
        )),
        map((certs): HttpEffectResponse => ({ body: certs, status: HttpStatus.OK }))
      )
    ),
  ))
)

const sign_static_schema = struct({
  key_pub: string(),
  lease: number()
})

export const sign_static_cert = (
  decrypt: (payload: string) => Observable<{ payload: unknown, nonce: string }>,
  encrypt: (nonce: string, payload: any) => string,
  sign: (lease: number, key_pub: string) => Observable<{ ca_crt: string, key_crt: string }>,
) => r.pipe(
  r.matchPath('/static'),
  r.matchType('POST'),
  r.useEffect(req => req.pipe(
    mergeMap(req => {
      console.log("Static IP requested.")
      if (typeof req.body !== 'string') {
        throw new HttpError('No authentication provided', HttpStatus.UNAUTHORIZED)
      }
      return decrypt(req.body)
    }),
    catchError(e => throwError(() => e instanceof HttpError ? e : new HttpError('Authentication failed', HttpStatus.FORBIDDEN))),
    map(struct({ payload: sign_static_schema, nonce: string() }).apply),
    catchError(e => throwError(() => e instanceof HttpError ? e : new HttpError('Invalid schema', HttpStatus.BAD_REQUEST))),
    mergeMap(({ payload, nonce }) =>
      sign(payload.lease, payload.key_pub).pipe(
        map(certs => encrypt(nonce, certs)),
        map((certs): HttpEffectResponse => ({ body: certs, status: HttpStatus.OK }))
      )
    ),
  ))
)
