/// <reference types="@types/jest"/>
import { IP } from '@nebula-dhcp/ip'
import { spawn } from 'pinospace'
import { defer, finalize, lastValueFrom, map, merge, mergeMapTo, NEVER, of, tap, throwError, timer } from 'rxjs'
import type { NebulaBinary } from './nebula-binary'
import { nebula_time } from './nebula-time'
import { nebula_sign, sign, sign_remote } from './sign'

function setup(response: string | Error) {
  const binary: NebulaBinary = jest.fn((...args: string[]) => defer(() => timer(100).pipe(map(() => {
    if (response instanceof Error) throw response
    return { stderr: '', stdout: response }
  }))))

  const temp_closed = jest.fn()

  const temp_files = jest.fn(() => merge(of('temp'), NEVER).pipe(finalize(temp_closed)))

  var f: any
  const files_data = {
    ca_crt: {
      path: 'ca_crt'
    },
    ca_key: {
      path: 'ca_key'
    },
    key_crt: {
      path: 'key_pri'
    },
    key_pub: {
      get: of('key.pub'),
      path: 'key_pub'
    },
    get: jest.fn(() => of({ key_crt: 'key.crt', ca_crt: 'ca.crt' })),
    set: jest.fn(() => of(void 0)),
    add: jest.fn(() => of(f)),
  }
  f = files_data

  const files = jest.fn(() => of(files_data))

  const log = spawn()

  const nebula_sign_called = jest.fn()
  const nebula_sign = jest.fn(() => of({ key_crt: 'key.crt', ca_crt: 'ca.crt' }).pipe(tap(nebula_sign_called)))

  const sign_called = jest.fn()
  const sign = jest.fn(x => x.pipe(map(x => (sign_called(x), { key_crt: 'key.crt', ca_crt: 'ca.crt' }))))

  return {
    binary,
    temp_files,
    temp_closed,
    sign,
    sign_called,
    nebula_sign_called,
    nebula_sign,
    files,
    files_data,
    log,
  }
}

const w = (ms: number) => new Promise<void>(k => setTimeout(k, ms))

describe('nebula_sign', () => {
  it(`Does not call when initialized`, async () => {
    const { binary, log, files, temp_files } = setup('')

    nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any)

    await w(100)

    expect(files).not.toBeCalled()
  })

  it(`Gets temp folder`, async () => {
    const { binary, log, files, temp_files } = setup('')

    await lastValueFrom(nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any))


    expect(temp_files).toBeCalledWith(expect.any(Object))
  })

  it(`Creates files from temp folder`, async () => {
    const { binary, log, files, temp_files } = setup('')

    await lastValueFrom(nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any))

    expect(files).toBeCalledWith('temp', expect.any(Object))
  })

  it(`Calls nebula binary`, async () => {
    const { binary, log, files, temp_files, files_data } = setup('')

    await lastValueFrom(nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any))

    expect(binary).toBeCalledWith('sign', '-ca-crt', files_data.ca_crt.path, '-ca-key', files_data.ca_key.path, '-in-pub', files_data.key_pub.path, '-name', 'name', '-duration', '0h0m10s', '-ip', '192.168.1.1/24', '-out-crt', files_data.key_crt.path)
  })

  it('Fails on nebula error', async () => {
    const { binary, log, files, temp_files } = setup(Error('any'))

    await expect(lastValueFrom(nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any))).rejects.toThrowError('Unable to sign certificate')
  })

  it('Fails on get certificate fail', async () => {
    const { binary, log, files, files_data, temp_files } = setup('')

    files_data.get.mockImplementationOnce(() => throwError(() => Error('')))

    await expect(lastValueFrom(nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any))).rejects.toThrowError('Unable to get signed certificate')
  })

  it('Returns certificates', async () => {
    const { binary, log, files, temp_files } = setup('')

    await expect(lastValueFrom(nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any))).resolves.toEqual({ key_crt: 'key.crt', ca_crt: 'ca.crt' })
  })

  it('Closes temp', async () => {
    expect.assertions(1)
    const { binary, log, files, temp_files, temp_closed } = setup('')
    await lastValueFrom(nebula_sign(binary, 'name', 10000, IP.from_cidr('192.168.1.1/24'), 'ca_crt', 'ca_key', 'key_pub', log, { files, temp_files, nebula_time } as any).pipe(
      mergeMapTo(timer(100)),
      map(() => expect(temp_closed).toBeCalled())
    ))
  })
})

describe('sign', () => {
  it('Does not call when initialized', async () => {
    const { binary, log, files, nebula_sign, nebula_sign_called } = setup('')

    sign(binary,files() as any, log, { nebula_sign } as any)('name', 10000, IP.from_cidr('192.168.1.1/24'), 'pub')

    await w(100)

    expect(nebula_sign).not.toBeCalled()
    expect(nebula_sign_called).not.toBeCalled()
  })

  it('Calls nebula_sign', async () => {
    const { binary, log, files, files_data, nebula_sign, nebula_sign_called } = setup('')

    const ip = IP.from_cidr('192.168.1.1/24')
    files_data.get.mockReturnValueOnce(of({ ca_crt: 'crt', ca_key: 'key' }) as any)

    await lastValueFrom(sign(binary,files() as any, log, { nebula_sign } as any)('name', 10000, ip, 'pub'))

    expect(nebula_sign).toBeCalledWith(binary, 'name', 10000, ip, 'crt', 'key', 'pub', expect.anything())
    expect(nebula_sign_called).toBeCalled()
  })


  it('Returns signed', async () => {
    const { binary, log, files, files_data, nebula_sign, nebula_sign_called } = setup('')

    const ip = IP.from_cidr('192.168.1.1/24')
    files_data.get.mockReturnValueOnce(of({ ca_crt: 'crt', ca_key: 'key' }) as any)

    await expect(lastValueFrom(sign(binary, files() as any, log, { nebula_sign } as any)('name', 10000, ip, 'pub'))).resolves.toEqual({ ca_crt: 'ca.crt', key_crt: 'key.crt' })
  })
})

describe('sign_remote', () => {
  it('Does not call when initialized', async () => {
    const { sign, log, files } = setup('')

    sign_remote(sign, log)(files() as any)

    await w(100)
    expect(sign).not.toBeCalled()
  })

  it('Calls sign', async () => {
    const { sign, sign_called, log, files } = setup('')

    await lastValueFrom(sign_remote(sign, log)(files() as any))

    expect(sign_called).toBeCalledWith('key.pub')
  })

  it('Fails on sign error', async () => {
    const { sign, log, files } = setup('')
    sign.mockReturnValueOnce(throwError(() => Error('')))

    await expect(lastValueFrom(sign_remote(sign, log)(files() as any))).rejects.toThrowError('Unable to sign certificate')
  })

  it('Calls add', async () => {
    const { sign, sign_called, log, files, files_data } = setup('')

    await lastValueFrom(sign_remote(sign, log)(files() as any))

    expect(files_data.add).toBeCalledWith({ key_crt: 'key.crt', ca_crt: 'ca.crt' })
  })

  it('Fails on add error', async () => {
    const { sign, log, files, files_data } = setup('')

    files_data.add.mockReturnValueOnce(throwError(() => Error('')))

    await expect(lastValueFrom(sign_remote(sign, log)(files() as any))).rejects.toThrowError('Unable to add signed certificates')
  })
})
