/// <reference types="@types/jest"/>
import { spawn } from 'pinospace'
import { defer, lastValueFrom, map, timer } from 'rxjs'
import type { File } from './file'
import { info } from './info'
import type { NebulaBinary } from './nebula-binary'

function setup(path: string, response: string | Error) {
  const binary: NebulaBinary = jest.fn((...args: string[]) => defer(() => timer(100).pipe(map(() => {
    if (response instanceof Error) throw response
    return { stderr: '', stdout: response }
  }))))

  const file: Pick<File, 'path'> = {
    path
  }

  const log = spawn()

  return {
    binary,
    file,
    log,
  }
}

const w = (ms: number) => new Promise<void>(k => setTimeout(k, ms))

const print_output = `{"details":{"groups":[],"ips":[],"isCa":true,"issuer":"","name":"test","notAfter":"2022-08-06T15:20:39Z","notBefore":"2021-08-06T15:20:39Z","publicKey":"064eecdc5f204128c9a1e19e16d278a310b46a3e29d19f5621598a4fe2b972d5","subnets":[]},"fingerprint":"be246137cf24435bd86b9b51784674ad6a1ff519ac0445ebcfc070bdb593e146","signature":"d1ccd26ae8bc370def2571738e6278afb47a66e3be3e4be1420253760f1cdd54919e9c2f6dd0daa7db662b8d161c85a191b9d9f00dd67465870190ae448f2a0d"}
{"details":{"groups":[],"ips":[],"isCa":true,"issuer":"","name":"test","notAfter":"2022-08-06T15:20:10Z","notBefore":"2021-08-06T15:20:10Z","publicKey":"ed6fe4b8c856fd170f060a325eeb62c0083336c538d025021e2ba5322aae2c8a","subnets":[]},"fingerprint":"fc2bffc4fb6c7767e956b1efff9ed356ad1b8cde7da1f5628914a3c115993aa1","signature":"e0cd6d07bb96a959f45459fbff284e614705b6f4654f469b7ab59eaaa462131b6e74c3c7fbaa7eacd1d169d91e9819132c45fc4df0ee3e5bfb133b010ba03407"}`

const print_result = [
  { "details": { "groups": [], "ips": [], "isCa": true, "issuer": "", "name": "test", "notAfter": new Date("2022-08-06T15:20:39Z"), "notBefore": new Date("2021-08-06T15:20:39Z"), "publicKey": "064eecdc5f204128c9a1e19e16d278a310b46a3e29d19f5621598a4fe2b972d5", "subnets": [] }, "fingerprint": "be246137cf24435bd86b9b51784674ad6a1ff519ac0445ebcfc070bdb593e146", "signature": "d1ccd26ae8bc370def2571738e6278afb47a66e3be3e4be1420253760f1cdd54919e9c2f6dd0daa7db662b8d161c85a191b9d9f00dd67465870190ae448f2a0d" },
  { "details": { "groups": [], "ips": [], "isCa": true, "issuer": "", "name": "test", "notAfter": new Date("2022-08-06T15:20:10Z"), "notBefore": new Date("2021-08-06T15:20:10Z"), "publicKey": "ed6fe4b8c856fd170f060a325eeb62c0083336c538d025021e2ba5322aae2c8a", "subnets": [] }, "fingerprint": "fc2bffc4fb6c7767e956b1efff9ed356ad1b8cde7da1f5628914a3c115993aa1", "signature": "e0cd6d07bb96a959f45459fbff284e614705b6f4654f469b7ab59eaaa462131b6e74c3c7fbaa7eacd1d169d91e9819132c45fc4df0ee3e5bfb133b010ba03407" }
]

it('Does not call when initialized', async () => {
  const { binary, file, log } = setup('certpath', '')

  info(binary, file, log)

  w(100)

  expect(binary).not.toBeCalled()
})

it('Calls when subscribed', async () => {
  const { binary, file, log } = setup('certpath', print_output)

  info(binary, file, log).subscribe()

  w(100)

  expect(binary).toBeCalledWith('print', '-path', 'certpath', '-json')
})

it('Parses output', async () => {
  const { binary, file, log } = setup('certpath', print_output)

  await expect(lastValueFrom(info(binary, file, log))).resolves.toEqual(print_result)
})

it('Parses output and clears extra whitespace', async () => {
  const { binary, file, log } = setup('certpath', print_output.split('\n').join('\n  \n     \n\n') + '\n')

  await expect(lastValueFrom(info(binary, file, log))).resolves.toEqual(print_result)
})

it('Fails on non JSON', async () => {
  const { binary, file, log } = setup('certpath', 'in{valid[')

  await expect(lastValueFrom(info(binary, file, log))).rejects.toThrowError('Failed to extract info')
})

it('Fails on invalid JSON', async () => {
  const { binary, file, log } = setup('certpath', '[]\n[]')

  await expect(lastValueFrom(info(binary, file, log))).rejects.toThrowError('Failed to extract info')
})
