/// <reference types="@types/jest"/>
import { init, db } from './db'
import { Memory } from '@starfuel/persistence-memory'
import { map, defer, firstValueFrom, from, lastValueFrom, mapTo, mergeMap, Observable, of, shareReplay, Subscription, timer } from 'rxjs'
import { IP } from '@nebula-dhcp/ip'
import { free, issue_dynamic, issue_static } from './pool'

const cleanup: Array<Subscription> = []

afterEach(() => cleanup.splice(0, cleanup.length).map(x => x.unsubscribe()))

const w = <T = undefined>(ms: number, result?: T) => new Promise(k => setTimeout(() => k(result), ms))

it('Full test', async () => {

  // IP address range to use. 192.168.1.0/29 - 192.168.1.7/29 (8 ip addresses).
  const addresses = IP.from_cidr('192.168.1.0/29')

  // IP addresses designated as "static" IP. These addresses will not be issued dynamically.
  const static_ip = [
    IP.from_cidr('192.168.1.0/29'),
    IP.from_cidr('192.168.1.1/29'),
    IP.from_cidr('192.168.1.7/29'),
    IP.from_cidr('192.168.1.6/29'),
  ]

  // Construct the database.
  const DB = defer(() => of(new Memory())).pipe(
    db,
    init(addresses, static_ip),
    shareReplay({ refCount: true, bufferSize: 1 })
  )

  // Keep the database alive for the test.
  cleanup.push(DB.subscribe())

  // Mock the signing function.
  function sign(name: string, lease: number, ip: IP, key_pub: string): Observable<{ ca_crt: string, key_crt: string }> {
    return timer(100).pipe(
      mapTo({ ca_crt: `ca:${name}:${lease}:${key_pub}:${ip.cidr}`, key_crt: `ky:${name}:${lease}:${key_pub}:${ip.cidr}` })
    )
  }

  // Setup the issuing functions.
  const issue = {
    dynamic: issue_dynamic(500, DB, sign),
    static: issue_static(DB, sign),
    free: free(DB)
  }

  // Get static IP 192.168.1.0.
  await expect(lastValueFrom(issue.static(10, '1', static_ip[0]))).resolves.toEqual({
    ca_crt: `ca:${static_ip[0].cidr}:10:1:${static_ip[0].cidr}`,
    key_crt: `ky:${static_ip[0].cidr}:10:1:${static_ip[0].cidr}`,
  })

  // Get dynamic IP 192.168.1.2.
  await expect(lastValueFrom(issue.dynamic('2'))).resolves.toEqual({
    ca_crt: `ca:192.168.1.2/29:500:2:192.168.1.2/29`,
    ip: "192.168.1.2/29",
    key_crt: `ky:192.168.1.2/29:500:2:192.168.1.2/29`,
  })

  // Get dynamic IP 192.168.1.3.
  await expect(lastValueFrom(issue.dynamic('3'))).resolves.toEqual({
    ca_crt: `ca:192.168.1.3/29:500:3:192.168.1.3/29`,
    ip: "192.168.1.3/29",
    key_crt: `ky:192.168.1.3/29:500:3:192.168.1.3/29`,
  })

  // Get dynamic IP 192.168.1.4.
  await expect(lastValueFrom(issue.dynamic('4'))).resolves.toEqual({
    ca_crt: `ca:192.168.1.4/29:500:4:192.168.1.4/29`,
    ip: "192.168.1.4/29",
    key_crt: `ky:192.168.1.4/29:500:4:192.168.1.4/29`,
  })

  // Get dynamic IP 192.168.1.5.
  await expect(lastValueFrom(issue.dynamic('5'))).resolves.toEqual({
    ca_crt: `ca:192.168.1.5/29:500:5:192.168.1.5/29`,
    ip: "192.168.1.5/29",
    key_crt: `ky:192.168.1.5/29:500:5:192.168.1.5/29`,
  })
  await w(100)

  // Fail to get next dynamic IP with new public key. No more addresses available.
  await expect(lastValueFrom(issue.dynamic('6'))).rejects.toThrowError('No IP addresses available to issue')

  // Free one IP
  await lastValueFrom(issue.free(IP.from_cidr('192.168.1.2/29')))

  // Get dynamic IP 192.168.1.2 after previous IP lease has expired.
  await expect(lastValueFrom(issue.dynamic('6'))).resolves.toEqual({
    ca_crt: `ca:192.168.1.2/29:500:6:192.168.1.2/29`,
    ip: "192.168.1.2/29",
    key_crt: `ky:192.168.1.2/29:500:6:192.168.1.2/29`,
  })

  // Wait for some IP leases to expire
  await w(100)

  // Get dynamic IP 192.168.1.3 after previous IP lease has expired.
  await expect(lastValueFrom(issue.dynamic('7'))).resolves.toEqual({
    ca_crt: `ca:192.168.1.3/29:500:7:192.168.1.3/29`,
    ip: "192.168.1.3/29",
    key_crt: `ky:192.168.1.3/29:500:7:192.168.1.3/29`,
  })

  // Get static IP 192.168.1.1.
  await expect(lastValueFrom(issue.static(1000, '8', static_ip[1]))).resolves.toEqual({
    ca_crt: `ca:${static_ip[1].cidr}:1000:8:${static_ip[1].cidr}`,
    key_crt: `ky:${static_ip[1].cidr}:1000:8:${static_ip[1].cidr}`,
  })

  // Get static IP 192.168.1.6.
  await expect(lastValueFrom(issue.static(60, '9', static_ip[2]))).resolves.toEqual({
    ca_crt: `ca:${static_ip[2].cidr}:60:9:${static_ip[2].cidr}`,
    key_crt: `ky:${static_ip[2].cidr}:60:9:${static_ip[2].cidr}`,
  })

  // Fail to get static IP 192.168.1.1 with new public key. IP not expired and keys mismatch.
  await expect(lastValueFrom(issue.static(110, '10', static_ip[1]))).rejects.toThrowError('Public key does not match cached key while existing session has not expired')

  // Get static IP 192.168.1.1 with old key and set lease to 0 to free up the IP.
  await expect(lastValueFrom(issue.static(0, '8', static_ip[1]))).resolves.toEqual({
    ca_crt: `ca:${static_ip[1].cidr}:0:8:${static_ip[1].cidr}`,
    key_crt: `ky:${static_ip[1].cidr}:0:8:${static_ip[1].cidr}`,
  })

  // Wait for some time to pass
  await w(10)

  // Get static IP 192.168.1.1 with new key.
  await expect(lastValueFrom(issue.static(110, '10', static_ip[1]))).resolves.toEqual({
    ca_crt: `ca:${static_ip[1].cidr}:110:10:${static_ip[1].cidr}`,
    key_crt: `ky:${static_ip[1].cidr}:110:10:${static_ip[1].cidr}`,
  })

  // Get static IP 192.168.1.7.
  await expect(lastValueFrom(issue.static(1, '11', static_ip[3]))).resolves.toEqual({
    ca_crt: `ca:${static_ip[3].cidr}:1:11:${static_ip[3].cidr}`,
    key_crt: `ky:${static_ip[3].cidr}:1:11:${static_ip[3].cidr}`,
  })

  // Free static IP 192.168.1.0.
  await lastValueFrom(issue.free(IP.from_cidr('192.168.1.0/29')))

  // Snapshot database
  const snap = await firstValueFrom(DB.pipe(
    map(x => x.snapshot())
  ))

  // Assert database state
  expect(snap.config.get('main').addresses.cidr).toBe(addresses.cidr)
  expect((snap.config.get('main').last_issue as null | { cidr: string })?.cidr).toBe('192.168.1.3/29')
  expect(
    snap.reserved
      .dump()
      .reduce((a, c)=> ({...a, [c.cidr]: c}),{} as Record<string, any>)
  ).toMatchObject({
    '192.168.1.0/29': {
      cidr: '192.168.1.0/29',
      type: 'static',
      key_pub: undefined,
      expiry: null
    },
    '192.168.1.1/29': {
      cidr: '192.168.1.1/29',
      type: 'static',
      key_pub: {
        key: '10'
      }
    },
    '192.168.1.2/29': {
      cidr: '192.168.1.2/29',
      type: 'dynamic',
      key_pub: {
        key: '6'
      }
    },
    '192.168.1.3/29': {
      cidr: '192.168.1.3/29',
      type: 'dynamic',
      key_pub: {
        key: '7'
      }
    },
    '192.168.1.4/29': {
      cidr: '192.168.1.4/29',
      type: 'dynamic',
      key_pub: {
        key: '4'
      }
    },
    '192.168.1.5/29': {
      cidr: '192.168.1.5/29',
      type: 'dynamic',
      key_pub: {
        key: '5'
      }
    },
    '192.168.1.6/29': {
      cidr: '192.168.1.6/29',
      type: 'static',
      key_pub: {
        key: '11'
      }
    },
    '192.168.1.7/29': {
      cidr: '192.168.1.7/29',
      type: 'static',
      key_pub: {
        key: '9'
      }
    },
  })
})
