/// <reference types="@types/jest"/>
import { IP } from './IP'

it('Loads ip address', () => {
  const ip = IP.from_cidr('192.168.1.32/24')
  expect(ip.cidr).toBe('192.168.1.32/24')
})

it('Gets next ip address', () => {
  const ip = IP.from_cidr('192.168.1.32/24')
  expect(ip.next.cidr).toBe('192.168.1.33/24')
})

it('Gets next ip address overflow', () => {
  const ip = IP.from_cidr('192.168.1.255/24')
  expect(ip.next.cidr).toBe('192.168.1.0/24')
})

it('Gets prev ip address', () => {
  const ip = IP.from_cidr('192.168.1.32/24')
  expect(ip.prev.cidr).toBe('192.168.1.31/24')
})

it('Gets prev ip address overflow', () => {
  const ip = IP.from_cidr('192.168.1.0/24')
  expect(ip.prev.cidr).toBe('192.168.1.255/24')
})

it('Gets ip address space', () => {
  const ip = IP.from_cidr('192.168.1.0/16')
  expect(ip.addresses).toBe(0xFFFF)
})

it('Is equal', () => {
  const ip = IP.from_cidr('192.168.1.0/16')
  expect(ip.equals(IP.from_cidr('192.168.1.0/16'))).toBeTruthy()
})

it('Is not equal (address)', () => {
  const ip = IP.from_cidr('192.168.1.0/16')
  expect(ip.equals(IP.from_cidr('192.168.1.1/16'))).toBeFalsy()
})

it('Is not equal (netmask)', () => {
  const ip = IP.from_cidr('192.168.1.0/16')
  expect(ip.equals(IP.from_cidr('192.168.1.0/15'))).toBeFalsy()
})
