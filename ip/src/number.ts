import type { B } from './types';


/**
 * Convert bits to number
 * @param bits Bits to convert
 */
export function number(bits: B[]): number {
  let pow = 0;
  let cum = 0;

  for (let i = bits.length; i--; i > 0) {
    cum += (bits[i] << pow);
    pow++;
  }

  return cum;
}
