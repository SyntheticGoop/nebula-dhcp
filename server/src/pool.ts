import { IP } from '@nebula-dhcp/ip'
import type { sign } from '@nebula-dhcp/nebula'
import type { Observable } from 'rxjs'
import { from, lastValueFrom, mergeMap } from 'rxjs'
import type { Database } from './db'




export function issue_dynamic(lease: number, db: Observable<Database>, signer: ReturnType<typeof sign>) {
  return function (key_pub: string) {
    return db.pipe(
      mergeMap(db => from(db.transaction('Issue Dynamic IP', async (db, session) => {
        const c = db.config.get({ config: 'main' })

        let prev = db.pub_key.get({ key: key_pub }, false);

        let ip: IP | null = prev ? IP.from_cidr(prev.ip.cidr) : null

        if (!ip) {
          const range = IP.from_bits(c.addresses.bits, c.addresses.netmask).all(c.last_issue ? IP.from_bits(c.last_issue.bits, c.last_issue.netmask) : null)

          for (const test of range) {
            const current = db.reserved.get({ cidr: test.cidr }, false)
            if (current?.type === 'static' || ((current?.expiry ?? 0) > Date.now())) continue

            ip = test
            break
          }
        }

        if (ip === null) return session.throw(Error('No IP addresses available to issue'))

        const { ca_crt, key_crt } = await lastValueFrom(signer(ip.cidr, lease, ip, key_pub))

        const db_ip = db.ip.set({ cidr: ip.cidr, bits: ip.bits, netmask: ip.netmask })

        db.reserved.set({
          cidr: db_ip.cidr,
          ip: db_ip,
          expiry: Date.now() + lease,
          key_pub: db.pub_key.set({
            key: key_pub,
            ip: db_ip,
          }),
          type: 'dynamic'
        })

        c.last_issue = db_ip

        return { ca_crt, key_crt, ip: db_ip.cidr }
      }))),
    )
  }
}


export function issue_static(db: Observable<Database>, signer: ReturnType<typeof sign>) {
  return function (lease: number, key_pub: string, ip: IP) {
    return db.pipe(
      mergeMap(db => from(db.transaction('Issue Static IP', async (db, session) => {

        const current = db.reserved.get({ cidr: ip.cidr }, false)

        if (current?.type === 'dynamic') return session.throw(Error('IP address is not in the static range'))

        if (((current?.expiry ?? 0) > Date.now()) && (current?.key_pub ? current.key_pub.key : '') !== key_pub) return session.throw(Error('Public key does not match cached key while existing session has not expired'))

        const { ca_crt, key_crt } = await lastValueFrom(signer(ip.cidr, lease, ip, key_pub))

        const db_ip = db.ip.set({ cidr: ip.cidr, bits: ip.bits, netmask: ip.netmask })

        db.reserved.set({
          cidr: ip.cidr,
          ip: db_ip,
          expiry: Date.now() + lease,
          key_pub: db.pub_key.set({
            key: key_pub,
            ip: db_ip,
          }),
          type: 'static'
        })

        return { ca_crt, key_crt }
      }))),
    )
  }
}


export function free(db: Observable<Database>) {
  return function (ip: IP) {
    return db.pipe(
      mergeMap(db => from(db.transaction('Issue Static IP', async db => {

        const current = db.reserved.get({ cidr: ip.cidr }, false)

        if (current) {
          current.expiry = null
          current.key_pub = undefined
        }
      }))),
    )
  }
}