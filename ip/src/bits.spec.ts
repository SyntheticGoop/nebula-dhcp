/// <reference types="@types/jest"/>
import { bits } from './bits'
import type { B } from './types'

it.each(
  Array.from({ length: 1024 }, (_, i) => [i, Array.from(i.toString(2)).map(x => +x) as any as B[]] as const)
)('Converts %s', (x, b) => expect(bits(x)).toEqual(b))

it('Pads bits', () => expect(bits(15, 8)).toEqual([0, 0, 0, 0, 1, 1, 1, 1]))