/// <reference types="@types/jest"/>
import { delay, firstValueFrom, lastValueFrom, map, mergeMap, Subscription } from 'rxjs'
import { Crypto, join_key, reply_decrypt, reply_encrypt, request_decrypt, request_encrypt, time_based_rotating_key } from './encryption'

const cleanup: Array<Subscription> = []

const w = (ms: number, reply: any = void 0) => new Promise(k => setTimeout(() => k(reply), ms))

afterEach(() => cleanup.splice(0, cleanup.length).map(x => x.unsubscribe()))

describe('time_based_rotating_key', () => {
  it('Generates keys immediately', async () => {
    const k = time_based_rotating_key(100)
    cleanup.push(k.subscribe())

    await expect(Promise.race([firstValueFrom(k), w(10, null)])).resolves.toEqual([expect.stringMatching(/^.{64}$/), expect.stringMatching(/^.{64}$/)])
  })

  it('Rotates keys at half interval', async () => {
    const k = time_based_rotating_key(100)
    cleanup.push(k.subscribe())
    const k1 = await firstValueFrom(k)
    await w(70)
    const k2 = await firstValueFrom(k)
    expect(k2).toEqual([k1[1], expect.stringMatching(/^.{64}$/)])
    await w(50)
    await expect(firstValueFrom(k)).resolves.toEqual([k2[1], expect.stringMatching(/^.{64}$/)])
  })
})

describe('request', () => {
  it('Encrypts request', async () => {
    const k = time_based_rotating_key(100)
    cleanup.push(k.subscribe())

    w(10)
    const encrypt = request_encrypt('SECRET', k.pipe(map(x => x[1])))

    const [, nonce] = await firstValueFrom(k)
    expect(JSON.parse(Crypto.decryptString(await lastValueFrom(encrypt('nonce', 'payload'),), join_key(nonce, 'SECRET')))).toEqual({
      nonce: 'nonce',
      payload: 'payload'
    })
  })

  it('Encrypts request with latest key', async () => {
    const k = time_based_rotating_key(100)
    cleanup.push(k.subscribe())

    w(110)
    const encrypt = request_encrypt('SECRET', k.pipe(map(x => x[1])))

    const [, nonce] = await firstValueFrom(k)
    expect(JSON.parse(Crypto.decryptString(await lastValueFrom(encrypt('nonce', 'payload'),), join_key(nonce, 'SECRET')))).toEqual({
      nonce: 'nonce',
      payload: 'payload'
    })
  })

  it('Decrypts encrypted request', async () => {
    const k = time_based_rotating_key(100)
    cleanup.push(k.subscribe())

    w(10)
    const encrypt = request_encrypt('SECRET', k.pipe(map(x => x[1])))

    const decrypt = request_decrypt('SECRET', k)

    await expect(
      lastValueFrom(
        encrypt('nonce', 'payload').pipe(
          mergeMap(encrypted => decrypt(encrypted))
        )
      )
    ).resolves.toEqual({
      nonce: 'nonce',
      payload: 'payload'
    })
  })

  it('Decrypts encrypted request on one rotation', async () => {
    const k = time_based_rotating_key(100)
    cleanup.push(k.subscribe())

    w(10)
    const encrypt = request_encrypt('SECRET', k.pipe(map(x => x[1])))

    const decrypt = request_decrypt('SECRET', k)

    await expect(
      lastValueFrom(
        encrypt('nonce', 'payload').pipe(
          delay(80),
          mergeMap(encrypted => decrypt(encrypted))
        )
      )
    ).resolves.toEqual({
      nonce: 'nonce',
      payload: 'payload'
    })
  })

  it('Fails decrypting encrypted request on two rotations', async () => {
    const k = time_based_rotating_key(100)
    cleanup.push(k.subscribe())

    w(10)
    const encrypt = request_encrypt('SECRET', k.pipe(map(x => x[1])))

    const decrypt = request_decrypt('SECRET', k)

    await expect(
      lastValueFrom(
        encrypt('nonce', 'payload').pipe(
          delay(130),
          mergeMap(encrypted => decrypt(encrypted))
        )
      )
    ).rejects.toThrowError('Failed to decrypt')
  })
})

describe('reply', () => {
  it('Encrypts reply', () => {
    expect(JSON.parse(Crypto.decryptString(reply_encrypt('SECRET')('nonce', 'payload'), join_key('nonce', 'SECRET')))).toEqual({
      payload: 'payload'
    })
  })

  it('Decrypts encrypted reply', () => {
    expect(reply_decrypt('SECRET')('nonce')(reply_encrypt('SECRET')('nonce', 'payload'))).toEqual({
      payload: 'payload'
    })
  })

  it('Fails decrypting reply', () => {
    expect(() => reply_decrypt('SECRET')('noce')(reply_encrypt('SECRET')('nonce', 'payload'))).toThrowError('Failed to decrypt')
  })
})
