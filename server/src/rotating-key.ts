import type { HttpEffectResponse } from '@marblejs/core';
import { r } from '@marblejs/core';
import type { Observable } from 'rxjs';
import { first, map, mergeMap } from 'rxjs';

export const rotating_key = (key: Observable<string>) => r.pipe(
  r.matchPath('/key'),
  r.matchType('GET'),
  r.useEffect(req => req.pipe(
    mergeMap(() =>
      key.pipe(
        first(),
        map((key): HttpEffectResponse => (
          console.log(`Key issued: ${key}`),
          ({ body: key, status: 200 })
        ))
      ))
  ))
)