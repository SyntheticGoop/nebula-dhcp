import { nonce, reply_decrypt, request_encrypt } from '@nebula-dhcp/encrypt'
import { files, keygen, nebula_binary, sign_remote } from '@nebula-dhcp/nebula'
import axios from 'axios'
import { string, struct } from 'hyperval'
import type { Logger } from 'pinospace'
import { defer, first, firstValueFrom, from, map, mergeMap, tap } from 'rxjs'

const certs = struct({
  payload: struct({
    ca_crt: string(),
    key_crt: string(),
    ip: string(),
  })
})


export function client(
  remote_fqdn: string,
  certs_folder_path: string,
  nebula_cert_path: string,
  secret: string,
  log: Logger
) {
  const rotating_key = defer(() => axios.get<string>(remote_fqdn + "/key")).pipe(
    map(res => res.data)
  )
  const dynamic_key = (nonce: string) => (payload: string) => from(axios.post<string>(remote_fqdn + "/dynamic", payload, {
    headers: {
      'Content-Type': 'text/plain'
    }
  })).pipe(
    map(res => reply_decrypt(secret)(nonce)(res.data))
  )

  const nebula_cert = nebula_binary(nebula_cert_path)

  return firstValueFrom(files(certs_folder_path, log).pipe(
    keygen(nebula_cert, log),
    sign_remote(key_pub =>
      key_pub.pipe(
        first(),
        mergeMap(key_pub => {
          let otp = nonce()
          return request_encrypt(secret, rotating_key)(otp, { key_pub }).pipe(
            mergeMap(dynamic_key(otp)),
            map(certs.apply),
            map(({ payload: { ip, ...rest } }) => (
              console.log(ip),
              rest
            ))
          )
        })
      ), log)
  ))
}