/// <reference types="@types/jest"/>
import { bit_decrement } from './bit-decrement'
import { number } from './number'
import type { B } from './types'

it.each(
  Array.from({ length: 1024 }, (_, i) => [i + 1] as const)
)('Converts %s', x => expect(number(bit_decrement([0, ...Array.from(x.toString(2)).map(x => +x)] as any as B[]))).toBe(x - 1))