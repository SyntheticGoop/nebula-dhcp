import type { Logger } from 'pinospace';
import type { Observable } from 'rxjs';
import { catchError, first, mapTo, mergeMap, of, throwError } from 'rxjs';
import type { Files } from './files';
import { files, temp_files } from './files';
import type { NebulaBinary } from './nebula-binary';

/**
 * Call the nebula binary to create a keypair in a temporary folder, retrieve and return the created keypair, then delete the temporary folder.
 * 
 * @param nebula Nebula Binary
 * @param log Logging function
 * @param di Dependency Injection
 */
export function nebula_keygen(
  nebula: NebulaBinary,
  log: Logger,
  di = { temp_files, files }
) {
  log = log.in('nebula_keygen')

  return di.temp_files(log).pipe(
    mergeMap(folder => di.files(folder, log)),
    mergeMap(({ key_pri, key_pub, get }) => nebula(
      'keygen',
      '-out-key', key_pri.path,
      '-out-pub', key_pub.path,
    ).pipe(
      catchError(() => throwError(() => (
        log.error('Unable to generate keypair'),
        Error('Unable to generate keypair')
      ))),
      mergeMap(() => get('key_pri', 'key_pub').pipe(
        catchError(error => throwError(() => (
          log.error({ error }, 'Unable to get generated keypair'),
          Error('Unable to get generated keypair')
        )))
      )),
    )),
    first(),
  )
}

/**
 * Generate new key and replace current key
 *
 * @param nebula Nebula Binary
 * @param log Logging function
 * @param di Dependency Injection
 */

export function set_new_key(
  nebula: NebulaBinary,
  log: Logger,
  di = { nebula_keygen }
) {
  log = log.in('set_new_key')

  const keygen = di.nebula_keygen(nebula, log)
  /**
   * Generate new key and replace current key
   *
   * @param current_files Current Files
   */
  return function (files: Observable<Files>): Observable<Files> {
    return files.pipe(
      mergeMap(files =>
        keygen.pipe(
          mergeMap(x => files.set(x).pipe(
            catchError(error => throwError(() => (
              log.error({ error }, 'Unable to set generated keypair'),
              Error('Unable to set generated keypair')
            )))
          )),
        )
      )
    )
  }
}


/**
 * Generate / retrieve key files
 *
 * @param nebula Nebula Binary
 * @param log Logging function
 * @param di Dependency Injection
 */
export function keygen(
  nebula: NebulaBinary,
  log: Logger,
  di = { set_new_key }
) {
  log = log.in('key')

  const create = di.set_new_key(nebula, log)

  /**
   * Generate / retrieve key files
   *
   * @param files Current Files
   */
  return function (files: Observable<Files>): Observable<Files> {
    return files.pipe(
      mergeMap(files => (
        log.trace('Getting current key'),
        files.get('key_pri', 'key_pub').pipe(
          mapTo(files),
          catchError(() => (
            log.debug('Key incomplete'),
            of(files).pipe(create)
          ))
        )
      ))
    )
  }
}
