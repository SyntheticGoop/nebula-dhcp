import type { Logger } from 'pinospace';
import type { Observable } from 'rxjs';
import { catchError, first, map, mergeMap, of, throwError } from 'rxjs';
import type { Files } from './files';
import { files, temp_files } from './files';
import { info } from './info';
import type { NebulaBinary } from './nebula-binary';
import { nebula_time } from './nebula-time';

/**
 * Call the nebula binary to create a certificate in a temporary folder, retrieve and return the created certs, then delete the temporary folder.
 * 
 * @param nebula Nebula Binary
 * @param name Name of certificate
 * @param lease Certificate lifespan
 * @param log Logging function
 * @param di Dependency Injection
 */
export function nebula_ca(
  nebula: NebulaBinary,
  name: string,
  lease: number,
  log: Logger,
  di = { temp_files, files, nebula_time }
) {
  log = log.in('nebula_ca')

  return di.temp_files(log).pipe(
    mergeMap(folder => di.files(folder, log)),
    mergeMap(({ ca_crt, ca_key, get }) => nebula(
      'ca',
      '-name', name,
      '-duration', di.nebula_time(lease),
      '-out-crt', ca_crt.path,
      '-out-key', ca_key.path,
    ).pipe(
      catchError(() => throwError(() => (
        log.error('Unable to generate CA'),
        Error('Unable to generate CA')
      ))),
      mergeMap(() => get('ca_crt', 'ca_key').pipe(
        catchError(error => throwError(() => (
          log.error({ error }, 'Unable to get generated CA'),
          Error('Unable to get generated CA')
        )))
      )),
    )),
    first(),
  )
}

/**
 * Generate new CA and append it to current CA
 *
 * @param nebula Nebula binary
 * @param name Name of certificate
 * @param lease Certificate lifespan
 * @param log Logging function
 * @param di Dependency Injection
 */
export function merge_new_ca(
  nebula: NebulaBinary,
  name: string,
  lease: number,
  log: Logger,
  di = { nebula_ca }
) {
  /**
   * Generate new CA and append it to current CA
   *
   * @param files Current Files
   */
  return function (files: Observable<Files>): Observable<Files> {
    log = log.in('merge_new_ca')

    return files.pipe(
      mergeMap(files => di.nebula_ca(nebula, name, lease, log).pipe(
        mergeMap(x => files.add(x).pipe(
          catchError(error => throwError(() => (
            log.error({ error }, 'Unable to add generated CA'),
            Error('Unable to add generated CA')
          )))
        ))
      ))
    )
  }
}


/**
 * Generate / retrieve CA files
 *
 * A new certificate will be issued whenever 
 *
 * @param nebula Nebula binary
 * @param name Name of certificate
 * @param lease Certificate remaining duration
 * @param log Logging function
 */
export function ca(
  nebula: NebulaBinary,
  name: string,
  lease: {
    max: number,
    min: number,
  },
  log: Logger,
  di = { merge_new_ca, info }
) {
  log = log.in('ca')

  const create = di.merge_new_ca(nebula, name, lease.max, log)

  /**
   * Generate / retrieve CA files
   *
   * Setting the regen argument to true will force a new CA every time this function is called.
   *
   * @param files Current Files
   */
  return function (files: Observable<Files>): Observable<Files> {
    return files.pipe(
      mergeMap(files => (
        log.trace('Getting current key'),
        files.get('ca_crt', 'ca_key').pipe(
          mergeMap(() =>
            di.info(nebula, files.ca_crt, log.in('info')).pipe(
              map(
                (certs): [Files, boolean] => [
                  files,
                  !certs.every(({ details: { notAfter, notBefore } }) => (notAfter.valueOf() - lease.min) < Date.now() || notBefore.valueOf() > Date.now())
                ]
              ),
              map(x => (
                !x[1] && log.debug('CA expired'),
                x
              )),
            )
          ),
          catchError(() => (
            log.debug('CA missing'),
            of<[Files, boolean]>([files, false])
          )),
        )
      )),
      mergeMap(([files, state]) => {
        if (state) return of(files)
        return of(files).pipe(create)
      })
    )
  }
}