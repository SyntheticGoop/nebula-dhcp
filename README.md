# Nebula DHCP

Nebula DHCP is an effort to provide a framework for automatically issuing [nebula](https://github.com/slackhq/nebula) certificates to dynamic clients.

## Prerequisites

You will require a recent version of [nodejs](https://nodejs.org) (^14) and [yarn](https://yarnpkg.com) (1-2) installed.

In the case where you do not wish, or are unable to install [yarn](https://yarnpkg.com), this repository uses `yarn 2` and thus includes
its own copy of `yarn` inside the `.yarn/releases` folder. You can run the provided script through `nodejs`.

## Running the program

There are two parts to the program, the `client` and the `server`.

The javascript executable files are located in:

|command|path|
|---|-
|server|./server/bin/main.js|
|client|./client/bin/main.js|

Additionally they are aliased in [yarn](https://yarnpkg.com) as:

|command|alias|
|---|-
|server|yarn server|
|client|yarn client|

You can execute the commands by running either the `path` through [node](https://nodejs.org) (It reads /bin/env node by default) or through
[yarn](https://yarnpkg.com) by executing the `alias` in your command line.

The interactive help menu can be accessed from either command by running the command followed by `--help` ie. (yarn server --help).

Debugging may be turned on by setting the environment variable `LOG="*"`.

**It is reccomended you pass the server through a reverse proxy with TLS for security.**

## Architecture

The program consists of these libraries.

|library|description|path|
|---|-|-
|@nebula-dhcp/ip|IP address and netmask parsing and walking|./ip|
|@nebula-dhcp/nebla|Wrapper around the nebula-cert binary, as well as the filesystem|./nebula|
|@nebula-dhcp/encrypt|Encryption for communication|./encrypt|
|@nebula-dhcp/server|Server|./server|
|@nebula-dhcp/client|Client|./client|
|rxjs|Reactive programming library for javascript||
|hyperval|Validation library for javascript||
|argvtype|Interactive CLI builder||
|starfuel|RocksDB based relational, transactional, event-sourced document store||
|marblejs|Reactive webserver framework||
|execa|Command line executor||
|string-crypto|Encryption||
|pinospace|Logging||
|axios|Ajax client||

Everything is written in [rxjs](https://rxjs.dev) style reactive programming, validated with [hyperval](https://gitlab.com/SyntheticGoop/hyperval), the CLI is build with [argvtype](https://gitlab.com/SyntheticGoop/argvtype) and the database uses [starfuel](https://gitlab.com/SyntheticGoop/starfuel).

The high level view of how everything works goes like this:
   1. A server is created.
   2. A client is started.
   3. The client looks in its certs folder for a public-private keypair and generates one if it does not exist, otherwise using it.
   4. The client asks the server for a time sensitive seed.
   5. The client encrypts the public key with the times sensitive seed, a client side nonce and the secret and sends it to the server.
   6. The server decrypts the payload.
   7. The server checks its database to see if the private key is currently holding an IP address.
   8. If it is, the server reuses the IP, otherwise the server checks its IP pool for th next free IP to issue.
   9. The server checks its certs folder for an existing CA and generates one if it does not exist, otherwise using it.
   10. The server signs a certificate, stores a record in its database, and encrypts it with the secret and nonce and replies the client.
   11. The client decrypts the certificate and stores it into the certificate folder.

## Bug reporting and feature requests

Bug reports and feature request MR/PRs are always welcome. Bug reports should include failing unit/integration tests if possible.

## Citations and acknowledgements

You may acknowledge the following, if the need arises.

```
Nanyang Technological University High Performance Computing Center
For providing development time to this project.
```

And

```
Emmanuel Lee
For writing and providing the software.
```

As well as any other libraries used in the making of this program as defined in the respective `package.json` manifests,
[nodejs](https://nodejs.org), [yarn](https://yarnpkg.com) and [nebula](https://github.com/slackhq/nebula)

