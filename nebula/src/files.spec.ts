/// <reference types="@types/jest"/>
import { spawn } from 'pinospace'
import { defer, delay, firstValueFrom, iif, lastValueFrom, map, mergeMap, of, shareReplay, Subscription, throwError } from 'rxjs'
import type { File } from './file'
import { files, temp_files } from './files'

const w = (ms: number) => new Promise<void>(k => setTimeout(k, ms))

function setup() {
  const mkdtemp = jest.fn((t: string): any => t + 'temp')
  const rm = jest.fn(async () => void 0)

  function file(path: string): File {
    const data = { data: null as null | string }

    const f: File = {
      set(text) {
        return defer(() => (data.data = text, of(f))).pipe(delay(100))
      },
      add(text) {
        return defer(() => (data.data = data.data ? `${data.data}\n${text}` : text, of(f))).pipe(delay(100))
      },
      get: iif(() => data.data !== null, defer(() => of(data.data as string)), throwError(() => Error('File Missing'))).pipe(delay(100)),
      path,
    }

    return f
  }

  const resolve = jest.fn((...x: any[]) => x.join('/'))

  return {
    mkdtemp,
    rm,
    resolve,
    file,
  }
}

const cleanup: Array<Subscription> = []

afterEach(() => cleanup.splice(0, cleanup.length).map(x => x.unsubscribe()))

describe('temp_folder', () => {
  it('Does not start on create', async () => {
    const { mkdtemp, rm } = setup()
    temp_files(spawn(), { mkdtemp, rm })
    await w(100)
    expect(mkdtemp).not.toBeCalledWith()
  })

  it('Calls make temp folder', async () => {
    const { mkdtemp, rm } = setup()
    await firstValueFrom(temp_files(spawn(), { mkdtemp, rm }))
    expect(mkdtemp).toBeCalledWith('temp')
  })

  it('Throws on make temp error', async () => {
    const { mkdtemp, rm } = setup()
    mkdtemp.mockImplementationOnce(() => { throw Error('') })
    await expect(lastValueFrom(temp_files(spawn(), { mkdtemp, rm }))).rejects.toThrowError('Failed to create temp folder')
  })

  it('Calls rm on disconnect', async () => {
    const { mkdtemp, rm } = setup()
    const lock = temp_files(spawn(), { mkdtemp, rm }).subscribe()
    cleanup.push(lock)

    await w(100)
    expect(rm).not.toBeCalled()

    lock.unsubscribe()

    await w(100)
    expect(rm).toBeCalledWith('temptemp', { recursive: true, force: true, maxRetries: 100 })
  })

  it('Returns temp folder', async () => {
    const { mkdtemp, rm } = setup()
    await expect(firstValueFrom(temp_files(spawn(), { mkdtemp, rm }))).resolves.toBe('temptemp')
  })
})

describe('files', () => {
  describe.each([
    ['key_pri', 'key.pri'],
    ['key_pub', 'key.pub'],
    ['key_crt', 'key.crt'],
    ['ca_key', 'ca.key'],
    ['ca_crt', 'ca.crt'],
  ] as const)('Gets, Sets, Adds, Paths %s', (key, path) => {
    it(`Path ${key}`, async () => {
      expect.assertions(1)
      const { file, resolve } = setup()
      lastValueFrom(files('./folder', spawn(), { file, resolve }).pipe(map(x => expect(x[key].path).toBe(`./folder/${path}`))))
    })

    it(`Gets empty ${key} errors`, async () => {
      const { file, resolve } = setup()
      await expect(lastValueFrom(files('./folder', spawn(), { file, resolve }).pipe(mergeMap(x => x[key].get)))).rejects.toThrowError()
    })

    it(`Sets-Gets new ${key}`, async () => {
      const { file, resolve } = setup()
      const f = files('./folder', spawn(), { file, resolve }).pipe(shareReplay({ refCount: false }))

      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].set('next'))))).resolves.toBeDefined()
      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].get)))).resolves.toBe('next')
    })

    it(`Sets-Gets old ${key}`, async () => {
      const { file, resolve } = setup()
      const f = files('./folder', spawn(), { file, resolve }).pipe(shareReplay({ refCount: false }))

      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].set('prev'))))).resolves.toBeDefined()
      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].set('next'))))).resolves.toBeDefined()
      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].get)))).resolves.toBe('next')
    })

    it(`Adds new ${key}`, async () => {
      const { file, resolve } = setup()
      const f = files('./folder', spawn(), { file, resolve }).pipe(shareReplay({ refCount: false }))

      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].add('next'))))).resolves.toBeDefined()
      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].get)))).resolves.toBe('next')
    })

    it(`Adds old ${key}`, async () => {
      const { file, resolve } = setup()
      const f = files('./folder', spawn(), { file, resolve }).pipe(shareReplay({ refCount: false }))

      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].add('prev'))))).resolves.toBeDefined()
      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].add('next'))))).resolves.toBeDefined()
      await expect(lastValueFrom(f.pipe(mergeMap(x => x[key].get)))).resolves.toBe('prev\nnext')
    })
  })

  it('Sets-Gets-Adds-Gets various', async () => {
    const { file, resolve } = setup()
    const f = files('./folder', spawn(), { file, resolve }).pipe(shareReplay({ refCount: false }))

    await expect(lastValueFrom(f.pipe(mergeMap(x => x.add({
      ca_crt: 'ca.crt',
      ca_key: 'ca.key',
      key_pri: 'key.pri',
    }))))).resolves.toBeDefined()

    await expect(lastValueFrom(f.pipe(mergeMap(x => x.get('ca_crt', 'key_pri'))))).resolves.toEqual({ ca_crt: 'ca.crt', key_pri: 'key.pri' })

    await expect(lastValueFrom(f.pipe(mergeMap(x => x.get('ca_key', 'key_pub'))))).rejects.toThrowError('Failed to get key_pub')


    await expect(lastValueFrom(f.pipe(mergeMap(x => x.add({
      ca_crt: '_',
      ca_key: '_',
      key_pri: '_',
      key_pub: '_',
    }))))).resolves.toBeDefined()

    await expect(lastValueFrom(f.pipe(mergeMap(x => x.get('ca_key', 'key_pub'))))).resolves.toEqual({ ca_key: 'ca.key\n_', key_pub: '_' })
    await expect(lastValueFrom(f.pipe(mergeMap(x => x.get('ca_key', 'key_pub', 'key_crt'))))).rejects.toThrowError('Failed to get key_crt')
  })
})