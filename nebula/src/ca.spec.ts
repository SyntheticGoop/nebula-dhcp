/// <reference types="@types/jest"/>
import { spawn } from 'pinospace'
import { defer, finalize, firstValueFrom, lastValueFrom, map, merge, mergeMapTo, NEVER, of, throwError, timer } from 'rxjs'
import { ca, merge_new_ca, nebula_ca } from './ca'
import { nebula_time } from './nebula-time'
import type { NebulaBinary } from './nebula-binary'

function setup(response: string | Error) {
  const binary: NebulaBinary = jest.fn((...args: string[]) => defer(() => timer(100).pipe(map(() => {
    if (response instanceof Error) throw response
    return { stderr: '', stdout: response }
  }))))

  const temp_closed = jest.fn()

  const temp_files = jest.fn(() => merge(of('temp'), NEVER).pipe(finalize(temp_closed)))

  var fd : any
  const files_data = {
    ca_crt: {
      path: 'ca_crt'
    },
    ca_key: {
      path: 'ca_key'
    },
    get: jest.fn(() => of({ ca_crt: 'crt', ca_key: 'key' })),
    add: jest.fn(() => of(fd))
  }
  fd = files_data

  const info_data = [
    {
      details: {
        notAfter: Date.now() + 100000,
        notBefore: Date.now() - 100000,
      }
    }
  ]
  const info = jest.fn(() => of(info_data))

  const files = jest.fn(() => of(files_data))

  const log = spawn()

  const nebula_ca = jest.fn(() => of({ ca_crt: 'crt', ca_key: 'key' }))

  const merge_new_ca_fn = jest.fn(x => x)
  const merge_new_ca = jest.fn(() => merge_new_ca_fn)

  return {
    binary,
    nebula_ca,
    temp_files,
    temp_closed,
    info,
    info_data,
    merge_new_ca,
    merge_new_ca_fn,
    files,
    files_data,
    log,
  }
}

const w = (ms: number) => new Promise<void>(k => setTimeout(k, ms))

describe('nebula_ca', () => {
  it(`Does not call when initialized`, async () => {
    const { binary, log, files, temp_files } = setup('')

    nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any)

    await w(100)

    expect(files).not.toBeCalled()
  })

  it(`Gets temp folder`, async () => {
    const { binary, log, files, temp_files } = setup('')

    await lastValueFrom(nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any))

    expect(temp_files).toBeCalledWith(expect.any(Object))
  })

  it(`Creates files from temp folder`, async () => {
    const { binary, log, files, temp_files } = setup('')

    await lastValueFrom(nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any))

    expect(files).toBeCalledWith('temp', expect.any(Object))
  })

  it(`Calls nebula binary`, async () => {
    const { binary, log, files, temp_files, files_data } = setup('')

    await lastValueFrom(nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any))

    expect(binary).toBeCalledWith('ca', '-name', 'name', '-duration', '0h0m1s', '-out-crt', files_data.ca_crt.path, '-out-key', files_data.ca_key.path)
  })

  it('Fails on nebula error', async () => {
    const { binary, log, files, temp_files } = setup(Error('any'))

    await expect(lastValueFrom(nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any))).rejects.toThrowError('Unable to generate CA')
  })

  it('Fails on get ca fail', async () => {
    const { binary, log, files, files_data, temp_files } = setup('')

    files_data.get = jest.fn(() => throwError(() => Error('')))

    await expect(lastValueFrom(nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any))).rejects.toThrowError('Unable to get generated CA')
  })

  it('Returns crt and key', async () => {
    const { binary, log, files, files_data, temp_files } = setup('')

    await expect(lastValueFrom(nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any))).resolves.toEqual({ ca_crt: 'crt', ca_key: 'key' })

    expect(files_data.get).toBeCalledWith('ca_crt', 'ca_key')
  })

  it('Closes temp', async () => {
    expect.assertions(1)
    const { binary, log, files, temp_files, temp_closed } = setup('')

    await lastValueFrom(nebula_ca(binary, 'name', 1000, log, { files, temp_files, nebula_time } as any).pipe(
      mergeMapTo(timer(100)),
      map(() => expect(temp_closed).toBeCalled())
    ))
  })
})

describe('merge_new_ca', () => {
  it('Does not call when initialized', async () => {
    const { binary, log, files, nebula_ca } = setup('')

    merge_new_ca(binary, 'name', 1000, log, { nebula_ca })(files() as any)

    await w(100)

    expect(nebula_ca).not.toBeCalled()
  })

  it('Calls nebula_ca', async () => {
    const { binary, log, files, nebula_ca } = setup('')

    await lastValueFrom(merge_new_ca(binary, 'name', 1000, log, { nebula_ca })(files() as any))

    expect(nebula_ca).toBeCalledWith(binary, 'name', 1000, expect.any(Object))
  })

  it('Adds CA', async () => {
    const { binary, log, files_data, files, nebula_ca } = setup('')

    await lastValueFrom(merge_new_ca(binary, 'name', 1000, log, { nebula_ca })(files() as any))

    expect(files_data.add).toBeCalledWith({ca_key: 'key', ca_crt: 'crt'})
  })

  it('Bubbles CA add failure', async () => {
    const { binary, log, files_data, files, nebula_ca } = setup('')

    files_data.add.mockReturnValueOnce(throwError(() => Error('')))

    await expect(lastValueFrom(merge_new_ca(binary, 'name', 1000, log, { nebula_ca })(files() as any))).rejects.toThrowError('Unable to add generated CA')
  })
})

describe('ca', () => {
  it('Does not call when initialized', async () => {
    const { binary, log, info, merge_new_ca, files, merge_new_ca_fn } = setup('')

    ca(binary, 'name', { max: 10000, min: 1000 }, log, { info, merge_new_ca } as any)(files() as any)

    await w(100)

    expect(merge_new_ca).toBeCalledWith(binary, 'name', 10000, expect.anything())
    expect(merge_new_ca_fn).not.toBeCalled()
    expect(info).not.toBeCalled()
  })

  it('Does not call new CA if CA does not need updating', async () => {
    const { binary, log, info, merge_new_ca, files, files_data, merge_new_ca_fn } = setup('')

    await lastValueFrom(ca(binary, 'name', { max: 10000, min: 1000 }, log, { info, merge_new_ca } as any)(files() as any))

    expect(info).toBeCalledWith(binary, files_data.ca_crt, expect.anything())
    expect(merge_new_ca_fn).not.toBeCalled()
  })

  it('Calls new CA if CA is missing crt', async () => {
    const { binary, log, info, merge_new_ca, files, files_data, merge_new_ca_fn } = setup('')

    files_data.get.mockImplementationOnce(() => throwError(() => Error('')))

    await lastValueFrom(ca(binary, 'name', { max: 10000, min: 1000 }, log, { info, merge_new_ca } as any)(files() as any))

    expect(info).not.toBeCalled()
    expect(merge_new_ca_fn).toBeCalled()
  })

  it('Does not call new CA if CA crt has one expired', async () => {
    const { binary, log, info, info_data, merge_new_ca, files, files_data, merge_new_ca_fn } = setup('')

    info_data.splice(0, info_data.length)
    info_data.push(
      {
        details: {
          notBefore: Date.now() - 100000,
          notAfter: Date.now() + 100000,
        }
      },
      {
        details: {
          notBefore: Date.now() + 100000,
          notAfter: Date.now() - 100000,
        }
      },
    )

    await lastValueFrom(ca(binary, 'name', { max: 10000, min: 1000 }, log, { info, merge_new_ca } as any)(files() as any))

    expect(info).toBeCalledWith(binary, files_data.ca_crt, expect.anything())
    expect(merge_new_ca_fn).not.toBeCalled()
  })

  it('Calls new CA if CA crt all expired', async () => {
    const { binary, log, info, info_data, merge_new_ca, files, files_data, merge_new_ca_fn } = setup('')

    info_data.splice(0, info_data.length)
    info_data.push(
      {
        details: {
          notBefore: Date.now() - 100000,
          notAfter: Date.now() + 900,
        }
      },
      {
        details: {
          notBefore: Date.now() + 100000,
          notAfter: Date.now() - 100000,
        }
      },
    )

    await lastValueFrom(ca(binary, 'name', { max: 10000, min: 1000 }, log, { info, merge_new_ca } as any)(files() as any))

    expect(info).toBeCalledWith(binary, files_data.ca_crt, expect.anything())
    expect(merge_new_ca_fn).toBeCalledWith(expect.anything())
    await expect(firstValueFrom(merge_new_ca_fn.mock.calls[0][0])).resolves.toEqual(files_data)
  })
})