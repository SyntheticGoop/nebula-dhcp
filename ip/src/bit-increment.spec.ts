/// <reference types="@types/jest"/>
import { bit_increment } from './bit-increment'
import { number } from './number'
import type { B } from './types'

it.each(
  Array.from({ length: 1024 }, (_, i) => [i] as const)
)('Converts %s', x => expect(number(bit_increment([0,...Array.from(x.toString(2)).map(x => +x) ]as any as B[]))).toBe(x + 1))