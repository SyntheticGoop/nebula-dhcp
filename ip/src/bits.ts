import type { B } from './types';


/**
 * Convert number to bits
 * @param number to bits
 * @param pad minimum bits
 */
export function bits(number: number, size: number = 0): B[] {
  const bits: B[] = []

  while (number > 1) {
    let next = number % 2 as B
    bits.push(next)
    number -= next
    number /= 2
  }
  bits.push(number as B)
  while (bits.length < size) {
    bits.push(0)
  }
  return bits.reverse()
}
