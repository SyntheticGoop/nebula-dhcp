import type { IP, IPBits, Netmask } from '@nebula-dhcp/ip'
import type { Persistence } from '@starfuel/interface'
import { array, augment, enums, Hyper, nullable, number, string } from 'hyperval'
import type { Observable } from 'rxjs'
import { mergeMap } from 'rxjs'
import type { OfDb, ToDb } from 'starfuel'
import { Collection, Db } from 'starfuel'

namespace Schema {
  export const ip = Collection.new({
    name: 'ip',
    schema: {
      cidr: string(),
      bits: augment(array(number(x => x === 0 || x === 1)), x => x.length === 32) as unknown as Hyper<IPBits>,
      netmask: number(x => x >= 1 || x <= 32) as unknown as Hyper<Netmask>,
    },
    key: ['cidr']
  })

  export const pub_key = Collection.new({
    name: 'pub_key',
    schema: {
      key: string(),
      ip: ip.link.one
    },
    key: ['key']
  })

  export const reserved = Collection.new({
    name: 'reserved',
    schema: {
      cidr: string(),
      ip: ip.link.one,
      type: enums('static', 'dynamic'),
      key_pub: pub_key.link.optional,
      expiry: nullable(number()),
    },
    key: ['cidr']
  })

  export const config = Collection.new({
    name: 'config',
    schema: {
      config: string(),
      addresses: ip.link.one,
      last_issue: ip.link.optional,
    },
    key: ['config'],
  })
}

export function db(persistence: Observable<Persistence>): Observable<Database> {
  return persistence.pipe(
    mergeMap(p => Db.load(Schema, p))
  )
}

export function init(addresses: IP, static_ip: IP[]) {
  return function (db: Observable<Database>): Observable<Database> {
    return db.pipe(
      mergeMap(db =>
        db.transaction('Set configuration', db => {
          db.config.set({
            config: 'main',
            addresses: db.ip.set({
              bits: addresses.bits,
              cidr: addresses.cidr,
              netmask: addresses.netmask,
            }),
            last_issue: void 0,
          })

          const static_ip_cidr = new globalThis.Set<string>(static_ip.map(x => x.cidr))
          for (const ip of static_ip) {
            db.reserved
              .filter(x => x.type === 'static' && !static_ip_cidr.has(x.cidr))
              .map(x => {
                x.type = 'dynamic'
                x.expiry = null
                x.key_pub = void 0
              })

            db.reserved.set({
              cidr: ip.cidr,
              type: 'static',
              ip: db.ip.set({
                bits: ip.bits,
                cidr: ip.cidr,
                netmask: ip.netmask
              }),
              expiry: null,
              key_pub: void 0,
            })
          }
        }).then(() => db)
      ),
    )
  }
}


export type Database = ToDb<typeof Schema>

export type Collections = OfDb<Database>