export type B = 0 | 1;
export type IPBits = [
  B, B, B, B, B, B, B, B,
  B, B, B, B, B, B, B, B,
  B, B, B, B, B, B, B, B,
  B, B, B, B, B, B, B, B
];
export type Netmask =
  0o01 | 0o02 | 0o03 | 0o04 | 0o05 | 0o06 | 0o07 |
  0o10 | 0o11 | 0o12 | 0o13 | 0o14 | 0o15 | 0o16 | 0o17 |
  0o20 | 0o21 | 0o22 | 0o23 | 0o24 | 0o25 | 0o26 | 0o27 |
  0o30 | 0o31 | 0o32 | 0o33 | 0o34 | 0o35 | 0o36 | 0o37;
