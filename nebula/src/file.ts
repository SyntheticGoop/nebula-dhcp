import { readFile, writeFile } from 'fs/promises';
import { resolve } from 'path';
import type { Logger } from 'pinospace';
import type { Observable } from 'rxjs';
import { catchError, concat, defer, filter, from, ignoreElements, map, mapTo, mergeMap, of, throwError, throwIfEmpty } from 'rxjs';

export type File = {
  /** File Path */
  path: string;

  /**
   * Sets data to file
   *
   * @param text File data
   * @returns `Promise<true>` File data set
   * @returns `Promise<false>` Unable to set file data
   */
  set(text: string): Observable<File>;

  /**
   * Appends data to file
   *
   * @param text File data
   * @returns `Promise<true>` File data set
   * @returns `Promise<false>` Unable to set file data
   */
  add(text: string): Observable<File>;

  /**
   * Get file data
   *
   * @returns `Promise<string>` File data
   * @returns `Promise<null>` No such file
   */
  get: Observable<string>;
};

/**
 * Load file from path
 * 
 * @param path Path to file
 * @param log Logging function
 * @param di Dependency Injection
 * 
 * @throws `Failed to load file` on `readFile` error
 * @throws `Loaded file is not text` if loaded data is not string
 */
export function get(path: string, log: Logger, di = { readFile }): Observable<string> {
  log = log.in('get').bind({ path })

  return defer(() => from(Promise.resolve(di.readFile(path, 'utf8')))).pipe(
    catchError(error => throwError(() => (
      log.error({ error }, 'Failed to load file'),
      Error('Failed to load file')
    ))),
    map(text => {
      if (typeof text !== 'string') throw (
        log.error('Loaded file is not text'),
        Error('Loaded file is not text')
      )

      return text
    }),
  )
}

/**
 * Set text to file
 * 
 * @param path Path to file
 * @param text Text to append
 * @param mode `append | set` Append adds data with newline to end, set replaces data
 * @param log Logging function
 * @param di Dependency Injection
 * 
 * @throws `Failed to write` on `writeFile` error
 * @throws `Failed to validate write` if written file does not match text to write
 */
export function set(path: string, text: string, mode: 'append' | 'set', log: Logger, di = { readFile, writeFile, get }): Observable<string> {
  log = log.in('set').bind({ path })

  const file = di.get(path, log)

  return file.pipe(
    catchError(() => (
      log.debug('File is does not already exist'),
      of('')
    )),
    map(prepend => (mode === 'append' && prepend.length > 0) ? `${prepend}\n${text}` : text),
    mergeMap(text => concat(
      from(Promise.resolve(di.writeFile(path, text, 'utf8'))).pipe(
        catchError(error => throwError(() => (
          log.error({ error }, 'Failed to write'),
          Error('Failed to write')
        ))),
        ignoreElements(),
      ),
      file.pipe(
        filter(next => next === text),
        throwIfEmpty(),
        catchError(() => throwError(() => (
          log.error('Failed to validate write'),
          Error('Failed to validate write')
        ))),
      )
    )),
  )
}

/**
 * Create new `File` object
 *
 * @param path File path
 * @param log Logging function
 * @param di Dependency Injection
 * @returns `File` object
 */
export function file(
  path: string,
  log: Logger,
  di = { readFile, writeFile, get, set }
): File {
  path = resolve(path);

  const get = di.get(path, log, di)

  const add = (text: string) =>
    di.set(path, text, 'append', log, di).pipe(
      mapTo({
        get,
        add,
        path,
        set,
      })
    )


  const set = (text: string) =>
    di.set(path, text, 'set', log, di).pipe(
      mapTo({
        get,
        add,
        path,
        set,
      })
    )

  return {
    get,
    add,
    path,
    set,
  }
}
