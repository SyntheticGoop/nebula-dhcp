import { mkdtemp, rm } from 'fs/promises'
import { resolve } from 'path'
import type { Logger } from 'pinospace'
import type { Observable } from 'rxjs'
import { catchError, defer, EMPTY, endWith, finalize, from, ignoreElements, map, merge, mergeMap, NEVER, of, reduce, throwError } from 'rxjs'
import type { File } from './file'
import { file } from './file'

/** Certificate files */
export type Files = {
  /** Private key */
  key_pri: File
  /** Public key */
  key_pub: File
  /** Key signed cert */
  key_crt: File
  /** CA key */
  ca_key: File
  /** CA cert */
  ca_crt: File
  /** Spawn a temp directory */
  temp: Observable<Omit<Files, 'temp'>>
  /** Get multiple files */
  get<Keys extends Array<'key_pri' | 'key_pub' | 'key_crt' | 'ca_key' | 'ca_crt'>>(...keys: Keys): Observable<{ [K in Keys extends Array<infer T> ? T : never]: string }>
  /** Add multiple files */
  add<Keys extends 'key_pri' | 'key_pub' | 'key_crt' | 'ca_key' | 'ca_crt'>(data: { [K in Keys]: string }): Observable<Files>
  /** Set multiple files */
  set<Keys extends 'key_pri' | 'key_pub' | 'key_crt' | 'ca_key' | 'ca_crt'>(data: { [K in Keys]: string }): Observable<Files>
}

/**
 * Helper for creating temp folder
 * 
 * The temp folder is cleaned up once the observable is disconnected
 * 
 * @param Logger Logging function
 * @param di Dependency Injection
 * 
 * @returns `Observable<string>` temp folder path
 * 
 * @throws `Failed to create temp folder` on `mkdtemp` error
 */
export function temp_files(
  log: Logger,
  di = { mkdtemp, rm }
): Observable<string> {
  log = log.in('temp_folder')
  return defer(() => (
    log.debug('Creating temp folder'),
    from(Promise.resolve(di.mkdtemp('temp')))
  )).pipe(
    mergeMap(temp => merge(
      of(temp),
      NEVER,
    ).pipe(
      finalize(() => from(Promise.resolve(di.rm(temp, { recursive: true, force: true, maxRetries: 100 }))).pipe(
        catchError(() => (
          log.warn({ temp }, 'Failed to remove temporary directory'),
          EMPTY
        ))
      ).subscribe())
    )),
    catchError(error => throwError(() => (
      log.error({ error }, 'Failed to create temp folder'),
      Error('Failed to create temp folder')
    )))
  )
}

/**
 * Helper for loading certificate files from a folder.
 * 
 * @param folder The folder in which to store and retrieve certificate files
 * @param Logger Logging function
 * @returns `Files` object
 */
export function files(
  folder: string,
  log: Logger,
  di = { file, resolve }
): Observable<Files> {
  log = log.in('files').bind({ folder })
  const each_files = {
    key_pri: di.file(di.resolve(folder, 'key.pri'), log.in('key_pri')),
    key_pub: di.file(di.resolve(folder, 'key.pub'), log.in('key_pub')),
    key_crt: di.file(di.resolve(folder, 'key.crt'), log.in('key_crt')),
    ca_key: di.file(di.resolve(folder, 'ca.key'), log.in('ca_key')),
    ca_crt: di.file(di.resolve(folder, 'ca.crt'), log.in('ca_crt')),
    temp: temp_files(log.in('temp')).pipe(
      mergeMap(folder => files(folder, log))
    ),
  }

  function get<Keys extends Array<'key_pri' | 'key_pub' | 'key_crt' | 'ca_key' | 'ca_crt'>>(...keys: Keys): Observable<{ [K in Keys extends Array<infer T> ? T : never]: string }> {
    return merge(
      ...keys.map(key => each_files[key].get.pipe(
        catchError(() => throwError(() => (
          log.error(`Failed to get ${key}`),
          Error(`Failed to get ${key}`)
        ))),
        map(val => ({ [key]: val }) as { [K in Keys extends Array<infer T> ? T : never]: string })
      ))
    ).pipe(
      reduce((a, c) => ({ ...a, ...c })),
    )
  }

  function add<Keys extends 'key_pri' | 'key_pub' | 'key_crt' | 'ca_key' | 'ca_crt'>(data: { [K in Keys]: string }): Observable<Files> {
    return from(Object.entries(data) as Array<[Keys, string]>).pipe(
      mergeMap(([key, val]) => each_files[key].add(val).pipe(
        catchError(() => throwError(() => (
          log.error(`Failed to add ${key}`),
          Error(`Failed to add ${key}`)
        ))),
      )),
      ignoreElements(),
      endWith({
        ...each_files,
        get,
        add,
        set,
      })
    )
  }

  function set<Keys extends 'key_pri' | 'key_pub' | 'key_crt' | 'ca_key' | 'ca_crt'>(data: { [K in Keys]: string }): Observable<Files> {
    return from(Object.entries(data) as Array<[Keys, string]>).pipe(
      mergeMap(([key, val]) => each_files[key].set(val).pipe(
        catchError(() => throwError(() => (
          log.error(`Failed to set ${key}`),
          Error(`Failed to set ${key}`)
        ))),
      )),
      ignoreElements(),
      endWith({
        ...each_files,
        get,
        add,
        set,
      })
    )
  }


  return defer(() => of({
    ...each_files,
    get,
    add,
    set,
  }))
}
