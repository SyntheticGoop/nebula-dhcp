import { time_based_rotating_key } from '@nebula-dhcp/encrypt'
import { IP } from '@nebula-dhcp/ip'
import { ca, files, nebula_binary, sign } from '@nebula-dhcp/nebula'
import { Level } from '@starfuel/persistence-level'
import type { Logger } from 'pinospace'
import { from, shareReplay } from 'rxjs'
import { db, init } from './db'
import { issue_dynamic } from './pool'
import { server } from './server'

export async function controller(
  certs_folder_path: string,
  nebula_cert_path: string,
  time_based_rotating_key_duration: number,
  name: string,
  ca_lease: number,
  cert_lease: number,
  ip_range: IP,
  static_ip: IP[],
  listen: {
    port: number,
    ip: string,
  },
  db_folder: string,
  secret: string,
  log: Logger
) {
  log = log.in('controller')
  const keys = time_based_rotating_key(time_based_rotating_key_duration).pipe(shareReplay({ refCount: false, bufferSize: 1 }))
  const nebula_cert = nebula_binary(nebula_cert_path)

  const certs_folder = files(certs_folder_path, log).pipe(
    ca(
      nebula_cert,
      name,
      {
        max: ca_lease,
        min: cert_lease
      },
      log
    )
  )

  let database = init(ip_range, static_ip)(db(from(Level.load(db_folder))))
  let issue = issue_dynamic(cert_lease, database, sign(nebula_cert, certs_folder, log))

  await (await server(secret, keys, issue)(listen.port, listen.ip))()
}
