/*
encryption protocol

c refers to client
s refers to server
> revers to sending of a message
: the action taken
~ is a message sent
e(key, message) encryption of a message
d(key, message) decryption of a message
k is the pre-shared key between the client and server
p(n) is a message
r is the time based rotating key on the server
n is the client nonce
? is a conditional branch
x is a connection forcefully closed

each new connection is started flushed left

c>s~ "get r"
  s>c~ r
    c: stores r

c: "request"
c: generate nonce (n)
c: p(0) = e(k + r, (n, "request"))
c>s~ p(0), "request"
  s: (n, "request") = d(k + r, p(0))
  s?x if unable to decrypt p(0), close connection and throttle IP
  s: "reply" = handle "request"
  s: p(1) = e(k + n, "reply")
  s>c~ p(1)
    c: "reply" = d(k + n, p(1))
    c: "reply"



Encryption is possibly vulnerable? I'm not a cryptographer.
*/

import { randomBytes } from 'crypto'
import { cast, HyperVal, string, struct, unknown } from 'hyperval'
import type { Observable } from 'rxjs'
import { catchError, first, interval, map, pairwise, share, shareReplay, startWith, throwError } from 'rxjs'
import StringCrypto from 'string-crypto'

/** Generate a random string */
export function nonce() {
  return randomBytes(48).toString('base64')
}

/**
 * Generates a pair of rotating random keys that expire in the given time.
 * 
 * The first key of the array is the previous key, which is still valid but should not be issued.
 * 
 * The second key of the array is the latest key.
 * 
 * @param expire Expiry interval of the key in milliseconds
 */
export function time_based_rotating_key(expire: number): Observable<[string, string]> {
  return interval(expire / 2).pipe(
    startWith(null),
    startWith(null),
    map(nonce),
    pairwise(),
    shareReplay({ refCount: true, bufferSize: 1 })
  )
}

export const Crypto = new StringCrypto({
  iterations: 12,
  digest: 'whirlpool'
})

export function join_key(nonce: string, secret: string) {
  return `${nonce}:${secret}`
}

export function request_encrypt(secret: string, rotating_key: Observable<string>): (nonce: string, payload: any) => Observable<string> {
  const encrypt = rotating_key.pipe(
    map(rotating_key => join_key(rotating_key, secret)),
    share()
  )

  return function (nonce, payload) {
    return encrypt.pipe(
      first(),
      map(key => Crypto.encryptString(JSON.stringify({ nonce, payload }), key))
    )
  }
}

const request_decrypt_schema = cast((x: string) => JSON.parse(x))(struct({
  nonce: string(),
  payload: unknown()
}))

export function request_decrypt(secret: string, rotating_key: Observable<[string, string]>): (encrypted_payload: string) => Observable<HyperVal<typeof request_decrypt_schema>> {
  const keys = rotating_key.pipe(
    map(([k1, k2]) => [join_key(k1, secret), join_key(k2, secret)])
  )

  return function (encrypted_payload) {
    return keys.pipe(
      map(([k2, k1]) => {
        try {
          return request_decrypt_schema.apply(Crypto.decryptString(encrypted_payload, k1))
        } catch { }
        return request_decrypt_schema.apply(Crypto.decryptString(encrypted_payload, k2))
      }),
      catchError(() => throwError(() => Error('Failed to decrypt'))),
      first()
    )
  }
}


export function reply_encrypt(secret: string) {
  return function (nonce: string, payload: any): string {
    return Crypto.encryptString(JSON.stringify({ payload }), join_key(nonce, secret))
  }
}

const reply_decrypt_schema = cast((x: string) => JSON.parse(x))(struct({
  payload: unknown()
}))

export function reply_decrypt(secret: string) {
  return function (nonce: string) {
    return function (encrypted_payload: string) {
      try {
        return reply_decrypt_schema.apply(Crypto.decryptString(encrypted_payload, join_key(nonce, secret)))
      } catch {
        throw Error('Failed to decrypt')
      }
    }
  }
}
