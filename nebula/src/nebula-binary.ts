import execa from 'execa';
import { resolve } from 'path';
import { Observable } from 'rxjs';

/** NebulaBinary */
export type NebulaBinary = (...args: string[]) => Observable<{ stdout: string; stderr: string; }>;

/**
 * Bind nebula binary path to executable callback.
 * 
 * @param binary_path Path to nebula binary
 */
export function nebula_binary(binary_path: string): NebulaBinary {
  binary_path = resolve(binary_path)

  return (...args: string[]) => new Observable(sub => {
    const call = execa(binary_path, args)
    call
      .then(state => (
        sub.next(state),
        sub.complete()
      ))
      .catch(error => sub.error(error))

    return () => call.cancel()
  })
}