import { string } from 'hyperval'
import { bit_decrement } from './bit-decrement'
import { bit_increment } from './bit-increment'
import { bits } from './bits'
import { number } from './number'
import type { B, IPBits, Netmask } from './types'

const ip_string_schema = string(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}\/([1-9]|([12][0-9])|(3[0-2]))$/)

/** IP address */
export class IP {
  constructor(
    readonly bits: IPBits,
    /** IP Netmask */
    readonly netmask: Netmask
  ) {
    this.equals = this.equals.bind(this)
    this.all = this.all.bind(this)
  }

  /** Netmask bits */
  private get net(): B[] {
    return this.bits.slice(0, this.netmask)
  }

  /** Address bits */
  private get address(): B[] {
    return this.bits.slice(this.netmask)
  }

  /** 
   * Slice IP address byte portion
   * 
   * @param pos Section to slice
   */
  private byte(pos: 0 | 1 | 2 | 3): number {
    return number(this.bits.slice(pos * 8, (pos + 1) * 8))
  }

  private ip_cache: null | string = null
  /** IP address without netmask */
  public get ip(): string {
    if (this.ip_cache === null) this.ip_cache = `${this.byte(0)}.${this.byte(1)}.${this.byte(2)}.${this.byte(3)}`
    return this.ip_cache
  }

  private cidr_cache: null | string = null
  /** IP address in CIDR notation */
  public get cidr() {
    if (this.cidr_cache === null) this.cidr_cache = `${this.ip}/${this.netmask}`
    return this.cidr_cache
  }

  /** Next IP address in range. Will overflow. Creates a new copy. */
  public get next(): IP {
    return new IP(
      [... this.net, ...bit_increment(this.address)] as IPBits,
      this.netmask
    )
  }

  /** Previous IP address in range. Will overflow. Creates a new copy. */
  public get prev(): IP {
    return new IP(
      [... this.net, ...bit_decrement(this.address)] as IPBits,
      this.netmask
    )
  }

  /** 
   * Iterate through all available IP addresses
   * 
   * @param start If provided, will iterate from this number onwards, otherwise will use start of current address range
   */
  public *all(start?: IP | null): Generator<IP> {
    let current = start ?? IP.from_bits([...this.net, ... this.address.map(() => 0)] as IPBits, this.netmask)
    for (let i = 0; i < this.addresses; i++) {
      yield current
      current = current.next
    }
  }

  /** Check if an IP is equal to current IP */
  public equals(ip: IP) {
    if (this.netmask !== ip.netmask) return false
    for (let i = 32; i--; i > 0) {
      if (this.bits[i] !== ip.bits[i]) return false
    }
    return true
  }

  /** Get the total number of IP addresses in this netmask */
  public get addresses(): number {
    return number(this.address.map(_ => 1))
  }

  /** Create IP from CIDR notation string */
  public static from_cidr(cidr: string): IP {
    cidr = cidr.trim()
    if (!ip_string_schema.is(cidr)) throw Error(`IP address ${cidr} is not in CIDR notation`)

    const [address, netmask] = cidr.split('/')
    const [a, b, c, d] = address.split(/\./g)

    return new IP(
      [
        ...bits(+a, 8),
        ...bits(+b, 8),
        ...bits(+c, 8),
        ...bits(+d, 8),
      ] as IPBits,
      +netmask as Netmask
    )
  }

  /** Create IP from bits and netmask */
  public static from_bits(bits: IPBits, netmask: Netmask) {
    return new IP(bits, netmask)
  }
}

