/// <reference types="@types/jest"/>

import { nebula_time } from './nebula-time'

it.each([
  ['0h0m1s', 1000],
  ['0h0m59s', 59000],
  ['0h1m0s', 60000],
  ['0h59m59s', 60000 * 59 + 59000],
  ['1h0m0s', 1000 * 60 * 60],
  ['1h59m59s', 1000 * 60 * 60 + 60000 * 59 + 59000],
  ['8760h0m0s', 1000 * 60 * 60 * 8760],
  ['87600h0m0s', 1000 * 60 * 60 * 87600],
  ['876000h0m0s', 1000 * 60 * 60 * 876000],
])('Converts %s', (converted, epoch) => {
  expect(nebula_time(epoch)).toBe(converted)
  expect(nebula_time(epoch + 999)).toBe(converted)
})