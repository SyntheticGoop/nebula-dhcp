#!/bin/env node
let resolve = require("path").resolve
let root = (...args) => resolve(__dirname, "../..", ...args)
require(root('.pnp.cjs')).setup()
require('ts-node').register({
  transpileOnly: true
})

let spawn = require('pinospace').spawn
let argvtype = require("argvtype")


const cmd = new argvtype.Command()
  .name("client")
  .description(`Spawn a client which communicates with a server to negotiate an nebula certificate.

The command will print out the IP received from the server.

Calling the command repeatedly while the previous certificate is still vaild\nwill force the server to reissue a new certificate with the same IP.`)
  .example("Regular usage", "/usr/bin/nebula-cert /etc/nebula/autocerts http://127.0.0.1:9012 some_long_and_secure_secret")
  .arguments(args => args
    .arg("binary", a => a
      .description("The 'nebula-cert' binary location.")
      .type("path", x => x)
      .required()
    )
    .arg("folder", a => a
      .description("The folder to store certificates in.")
      .type("path", x => x)
      .required()
    )
    .arg("fqdn", a => a
      .description("The server's fully qualified domain name.")
      .type("url", x => x)
      .required()
    )
    .arg("secret", a => a
      .description("A shared secret between the server and the client.")
      .type("string", x => x)
      .required()
    )
  ).action(async ({ args: { binary, folder, fqdn, secret } }) => {


    await require("../src/main").client(
      fqdn,
      folder,
      binary,
      secret,
      spawn("cli")
    )
  })

argvtype.cli(process.argv.slice(2), cmd)