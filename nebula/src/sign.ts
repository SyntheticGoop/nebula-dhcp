import type { IP } from '@nebula-dhcp/ip';
import type { Logger } from 'pinospace';
import type { Observable } from 'rxjs';
import { catchError, first, mapTo, mergeMap, throwError } from 'rxjs';
import type { Files } from './files';
import { files, temp_files } from './files';
import type { NebulaBinary } from './nebula-binary';
import { nebula_time } from './nebula-time';

/**
 * Call the nebula binary to sign a certificate in a temporary folder, retrieve and return the created certificate and accompanying ca certificate, then delete the temporary folder.
 * 
 * @param nebula Nebula Binary
 * @param name Certificate Name
 * @param lease Certificate Duration
 * @param ip IP address to use
 * @param ca_crt CA certificate
 * @param ca_key CA key
 * @param key_pub Public key
 * @param log Logging function
 * @param di Dependency Injection
 */
export function nebula_sign(
  nebula: NebulaBinary,
  name: string,
  lease: number,
  ip: IP,
  ca_crt: string,
  ca_key: string,
  key_pub: string,
  log: Logger,
  di = { temp_files, files, nebula_time }
) {
  log = log.in('nebula_sign')

  return di.temp_files(log).pipe(
    mergeMap(folder => di.files(folder, log)),
    mergeMap(files => files.set({ ca_crt, ca_key, key_pub }).pipe(
      catchError(error => throwError(() => (
        log.error({ error }, 'Unable to store signing files in temp folder'),
        Error('Unable to store signing files in temp folder')
      )))
    ).pipe(mapTo(files))),
    mergeMap(({ ca_crt, ca_key, key_pub, key_crt, get }) => nebula(
      'sign',
      '-ca-crt', ca_crt.path,
      '-ca-key', ca_key.path,
      '-in-pub', key_pub.path,
      '-name', name,
      '-duration', di.nebula_time(lease),
      '-ip', ip.cidr,
      '-out-crt', key_crt.path
    ).pipe(
      catchError(() => throwError(() => (
        log.error('Unable to sign certificate'),
        Error('Unable to sign certificate')
      ))),
      mergeMap(() => get('key_crt', 'ca_crt').pipe(
        catchError(error => throwError(() => (
          log.error({ error }, 'Unable to get signed certificates'),
          Error('Unable to get signed certificates')
        )))
      )),
    )),
    first(),
  )
}

/**
 * Sign certificates
 * 
 * @param nebula Nebula Binary
 * @param files Files
 * @param log Logging function
 * @param di Dependency Injection
 */
export function sign(
  nebula: NebulaBinary,
  files: Observable<Files>,
  log: Logger,
  di = { nebula_sign }
) {
  log = log.in('sign')

  const get_ca = files.pipe(
    mergeMap(({ get }) =>
      get('ca_crt', 'ca_key').pipe(
        catchError(error => throwError(() => (
          log.error({ error }, 'Unable to get signing files'),
          Error('Unable to get signing files')
        )))
      )
    )
  )
  /**
   * Sign certificates
   * 
   * @param name Certificate Name
   * @param lease Certificate Duration
   * @param ip IP address to use
   * @param key_pub Public key
   */
  return function (
    name: string,
    lease: number,
    ip: IP,
    key_pub: string,
  ) {
    return get_ca.pipe(
      mergeMap(({ ca_crt, ca_key }) =>
        di.nebula_sign(
          nebula,
          name,
          lease,
          ip,
          ca_crt,
          ca_key,
          key_pub,
          log
        )
      )
    )
  }
}


/**
 * Sign certificates with remote callback
 * 
 * @param sign Operator function that takes a public key string and returns signed certificates
 * @param log Logging function
 * @param di Dependency Injection
 */
export function sign_remote(
  sign: (pub: Observable<string>) => Observable<{ ca_crt: string; key_crt: string }>,
  log: Logger
) {
  log = log.in('sign_remote')
  /**
   * Sign certificates with remote callback
   * 
   * @param files Files
   */
  return function (files: Observable<Files>) {
    return files.pipe(
      mergeMap(({ key_pub, add }) =>
        key_pub.get.pipe(
          sign,
          catchError(error => throwError(() => (
            log.error({ error }, 'Unable to sign certificate'),
            Error('Unable to sign certificate')
          ))),
          mergeMap(({ ca_crt, key_crt }) => add({ ca_crt, key_crt }).pipe(
            catchError(error => throwError(() => (
              log.error({ error }, 'Unable to add signed certificates'),
              Error('Unable to add signed certificates')
            )))
          )),
        )
      )
    );
  };
}
