/// <reference types="@types/jest"/>
import { number } from './number'
import type { B } from './types'

it.each(
  Array.from({ length: 1024 }, (_, i) => [i, Array.from(i.toString(2).padStart(16, '0')).map(x => +x) as any as B[]] as const)
)('Converts %s', (x, bits) => expect(number(bits)).toBe(x))