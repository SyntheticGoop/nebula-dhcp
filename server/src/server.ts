import { createServer, httpListener } from '@marblejs/core';
import { bodyParser$ } from '@marblejs/middleware-body';
import { reply_encrypt, request_decrypt } from '@nebula-dhcp/encrypt';
import type { Observable } from 'rxjs';
import { map } from 'rxjs';
import { rotating_key } from './rotating-key';
import { sign_dynamic_cert } from './sign';

export function server(
  secret: string,
  time_based_rotating_key: Observable<[string, string]>,
  sign: (key_pub: string) => Observable<{ ca_crt: string, key_crt: string, ip: string }>
) {

  const decrypt = request_decrypt(secret, time_based_rotating_key)
  const encrypt = reply_encrypt(secret)

  const listener = httpListener({
    middlewares: [
      bodyParser$()
    ],
    effects: [
      rotating_key(time_based_rotating_key.pipe(map(x => x[1]))),
      sign_dynamic_cert(decrypt, encrypt, sign)
    ],
  });

  return function (port: number, ip: string) {
    return createServer({
      port: port,
      hostname: ip,
      listener,
    });
  }
}
