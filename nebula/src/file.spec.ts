/// <reference types="@types/jest"/>
import { spawn } from 'pinospace'
import { defer, lastValueFrom, mergeMap, of, throwError } from 'rxjs'
import { get, set } from './file'

const w = (ms: number) => new Promise<void>(k => setTimeout(k, ms))

function setup() {
  const file = { text: 'readFile' }
  const readFile = jest.fn((): any => Promise.resolve(file.text))
  const writeFile = jest.fn((_, next) => (file.text = next, Promise.resolve(void 0)))
  const get = jest.fn(() => defer(() => of(file.text)))
  return {
    readFile,
    writeFile,
    get,
    file,
  }
}

describe('get', () => {
  it('Does not start on create', async () => {
    const { readFile } = setup()
    get('path', spawn(), { readFile } as any)
    await w(100)
    expect(readFile).not.toBeCalledWith()
  })

  it('Calls read', async () => {
    const { readFile } = setup()
    await lastValueFrom(get('path', spawn(), { readFile } as any))
    expect(readFile).toBeCalledWith('path', 'utf8')
  })

  it('Loads file', async () => {
    const { readFile } = setup()
    await expect(lastValueFrom(get('path', spawn(), { readFile } as any))).resolves.toBe('readFile')
  })

  it('Loads file if return is sync', async () => {
    const { readFile } = setup()
    readFile.mockReturnValueOnce('')
    await expect(lastValueFrom(get('path', spawn(), { readFile } as any))).resolves.toBe('')
  })

  it('Throws on load failure', async () => {
    const { readFile } = setup()
    readFile.mockRejectedValueOnce(Error(''))
    await expect(lastValueFrom(get('path', spawn(), { readFile } as any))).rejects.toThrowError('Failed to load file')
  })

  it('Throws on non string file', async () => {
    const { readFile } = setup()
    readFile.mockReturnValueOnce(0)
    await expect(lastValueFrom(get('path', spawn(), { readFile } as any))).rejects.toThrowError('Loaded file is not text')
  })
})

describe('et', () => {
  it('Does not read start on create', async () => {
    const { readFile, writeFile, get } = setup()
    set('path', 'write', 'append', spawn(), { readFile, writeFile, get } as any)
    await w(100)
    expect(get).toBeCalledWith('path', expect.any(Object))
    expect(readFile).not.toBeCalled()
  })

  it('Proceeds on get failure', async () => {
    const { readFile, writeFile, get, file } = setup()
    let i = 0
    get.mockImplementation(() => defer(() => of(i++)).pipe(mergeMap(i => i === 0 ? throwError(() => Error('')) : of(file.text))))
    await expect(lastValueFrom(set('path', 'write', 'append', spawn(), { readFile, writeFile, get } as any))).resolves.toBeDefined()
  })

  it('Writes updated file (append)', async () => {
    const { readFile, writeFile, get } = setup()
    await lastValueFrom(set('path', 'write', 'append', spawn(), { readFile, writeFile, get } as any))
    expect(writeFile).toBeCalledWith('path', 'readFile\nwrite', 'utf8')
  })

  it('Writes updated file (set)', async () => {
    const { readFile, writeFile, get } = setup()
    await lastValueFrom(set('path', 'write', 'set', spawn(), { readFile, writeFile, get } as any))
    expect(writeFile).toBeCalledWith('path', 'write', 'utf8')
  })

  it('Throws on write failure', async () => {
    const { readFile, writeFile, get } = setup()
    writeFile.mockImplementationOnce(() => Promise.reject(Error('')))
    await expect(lastValueFrom(set('path', 'write', 'append', spawn(), { readFile, writeFile, get } as any))).rejects.toThrowError('Failed to write')
  })

  it('Throws on write mismatch', async () => {
    const { readFile, writeFile, get } = setup()
    writeFile.mockImplementationOnce(async () => void 0)
    await expect(lastValueFrom(set('path', 'write', 'append', spawn(), { readFile, writeFile, get } as any))).rejects.toThrowError('Failed to validate write')
  })
})