import { B } from './types';

/**
 * Decrement bits by 1
 * 
 * @param bits Bits to decrement
 */
export function bit_decrement(bits: B[]): B[] {
  bits = bits.slice();
  for (let i = bits.length; i--; i > 0) {
    if (bits[i] === 0) {
      bits[i] = 1;
    } else {
      bits[i] = 0;
      break;
    }
  }
  return bits;
}
