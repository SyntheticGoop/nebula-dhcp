import { B } from './types';

/**
 * Increment bits by 1
 * 
 * @param bits Bits to increment
 */
export function bit_increment(bits: B[]): B[] {
  bits = bits.slice();
  for (let i = bits.length; i--; i > 0) {
    if (bits[i] === 1) {
      bits[i] = 0;
    } else {
      bits[i] = 1;
      break;
    }
  }
  return bits;
}
