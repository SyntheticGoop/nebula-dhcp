import { alter, array, boolean, cast, HyperVal, object, string } from 'hyperval';
import type { Logger } from 'pinospace';
import type { Observable } from 'rxjs';
import { catchError, defer, map, throwError } from 'rxjs';
import type { File } from './file';
import type { NebulaBinary } from './nebula-binary';

const date = alter((x: string) => new Date(x))
const cert_info_schema = cast(
  (x: string) => x
    .split('\n')
    .map(x => x.trim())
    .filter(x => x.length > 0)
    .map(x => JSON.parse(x))
)(
  array(
    object({
      details: object({
        groups: array(string()),
        ips: array(string()),
        isCa: boolean(),
        issuer: string(),
        name: string(),
        notAfter: date(string()),
        notBefore: date(string()),
        publicKey: string(),
        subnets: array(string())
      }),
      fingerprint: string(),
      signature: string(),
    })
  )
)

export type CertInfo = HyperVal<typeof cert_info_schema>

/**
 * Extract certificate info
 *
 * @param nebula Nebula binary
 * @param cert Certificate string
 * @param log Logging function
 */

export function info(
  nebula: NebulaBinary,
  cert: Pick<File, 'path'>,
  log: Logger
): Observable<CertInfo> {
  defer

  return defer(() => nebula(
    'print',
    '-path', cert.path,
    '-json'
  )).pipe(
    map(({ stdout }) => cert_info_schema.apply(stdout)),
    catchError(() => throwError(() => (
      log.error('Failed to extract info'),
      Error('Failed to extract info')
    ))),
  )
}