/// <reference types="@types/jest"/>
import { spawn } from 'pinospace'
import { defer, finalize, lastValueFrom, map, merge, mergeMapTo, NEVER, of, tap, throwError, timer } from 'rxjs'
import { keygen, nebula_keygen, set_new_key } from './keygen'
import { nebula_time } from './nebula-time'
import type { NebulaBinary } from './nebula-binary'

function setup(response: string | Error) {
  const binary: NebulaBinary = jest.fn((...args: string[]) => defer(() => timer(100).pipe(map(() => {
    if (response instanceof Error) throw response
    return { stderr: '', stdout: response }
  }))))

  const temp_closed = jest.fn()

  const temp_files = jest.fn(() => merge(of('temp'), NEVER).pipe(finalize(temp_closed)))

  const files_data = {
    key_pri: {
      path: 'key_pri'
    },
    key_pub: {
      path: 'key_pub'
    },
    get: jest.fn(() => of({ key_pri: 'pri', key_pub: 'pub' })),
    set: jest.fn(() => of(void 0)),
  }

  const files = jest.fn(() => of(files_data))

  const log = spawn()

  const nebula_keygen_called = jest.fn()
  const nebula_keygen = jest.fn(() => of({ key_pri: 'pri', key_pub: 'pub' }).pipe(tap(nebula_keygen_called)))

  const set_new_key_fn = jest.fn(x => x)
  const set_new_key = jest.fn(() => set_new_key_fn)

  return {
    binary,
    temp_files,
    temp_closed,
    set_new_key,
    set_new_key_fn,
    nebula_keygen,
    nebula_keygen_called,
    files,
    files_data,
    log,
  }
}

const w = (ms: number) => new Promise<void>(k => setTimeout(k, ms))

describe('nebula_keygen', () => {
  it(`Does not call when initialized`, async () => {
    const { binary, log, files, temp_files } = setup('')

    nebula_keygen(binary, log, { files, temp_files, nebula_time } as any)

    await w(100)

    expect(files).not.toBeCalled()
  })

  it(`Gets temp folder`, async () => {
    const { binary, log, files, temp_files } = setup('')

    await lastValueFrom(nebula_keygen(binary, log, { files, temp_files, nebula_time } as any))

    expect(temp_files).toBeCalledWith(expect.any(Object))
  })

  it(`Creates files from temp folder`, async () => {
    const { binary, log, files, temp_files } = setup('')

    await lastValueFrom(nebula_keygen(binary, log, { files, temp_files, nebula_time } as any))

    expect(files).toBeCalledWith('temp', expect.any(Object))
  })

  it(`Calls nebula binary`, async () => {
    const { binary, log, files, temp_files, files_data } = setup('')

    await lastValueFrom(nebula_keygen(binary, log, { files, temp_files, nebula_time } as any))

    expect(binary).toBeCalledWith('keygen', '-out-key', files_data.key_pri.path, '-out-pub', files_data.key_pub.path)
  })

  it('Fails on nebula error', async () => {
    const { binary, log, files, temp_files } = setup(Error('any'))

    await expect(lastValueFrom(nebula_keygen(binary, log, { files, temp_files, nebula_time } as any))).rejects.toThrowError('Unable to generate keypair')
  })

  it('Fails on get keypair fail', async () => {
    const { binary, log, files, files_data, temp_files } = setup('')

    files_data.get.mockImplementationOnce(() => throwError(() => Error('')))

    await expect(lastValueFrom(nebula_keygen(binary, log, { files, temp_files, nebula_time } as any))).rejects.toThrowError('Unable to get generated keypair')
  })

  it('Returns pri and pub', async () => {
    const { binary, log, files, temp_files } = setup('')

    await expect(lastValueFrom(nebula_keygen(binary, log, { files, temp_files, nebula_time } as any))).resolves.toEqual({ key_pri: 'pri', key_pub: 'pub' })
  })

  it('Closes temp', async () => {
    expect.assertions(1)
    const { binary, log, files, temp_files, temp_closed } = setup('')

    await lastValueFrom(nebula_keygen(binary, log, { files, temp_files, nebula_time } as any).pipe(
      mergeMapTo(timer(100)),
      map(() => expect(temp_closed).toBeCalled())
    ))
  })
})

describe('set_new_key', () => {
  it('Does not call when initialized', async () => {
    const { binary, log, files, nebula_keygen, nebula_keygen_called } = setup('')

    set_new_key(binary, log, { nebula_keygen })(files() as any)

    await w(100)

    expect(nebula_keygen).toBeCalledWith(binary, expect.anything())
    expect(nebula_keygen_called).not.toBeCalled()
  })

  it('Calls nebula_keygen', async () => {
    const { binary, log, files, nebula_keygen, nebula_keygen_called } = setup('')

    await lastValueFrom(set_new_key(binary, log, { nebula_keygen })(files() as any))

    await w(100)

    expect(nebula_keygen_called).toBeCalled()
  })

  it('Sets keypair', async () => {
    const { binary, log, files, nebula_keygen, files_data } = setup('')

    await lastValueFrom(set_new_key(binary, log, { nebula_keygen })(files() as any))

    await w(100)

    expect(files_data.set).toBeCalledWith({ key_pri: 'pri', key_pub: 'pub' })
  })

  it('Bubbles keypair set failure', async () => {
    const { binary, log, files, nebula_keygen, files_data } = setup('')

    files_data.set.mockReturnValueOnce(throwError(() => Error('')))

    await expect(lastValueFrom(set_new_key(binary, log, { nebula_keygen })(files() as any))).rejects.toThrowError('Unable to set generated keypair')
  })
})

describe('ca', () => {
  it('Does not call when initialized', async () => {
    const { binary, log, files, set_new_key, set_new_key_fn } = setup('')

    keygen(binary, log, { set_new_key })(files() as any)

    await w(100)

    expect(set_new_key).toBeCalledWith(binary, expect.anything())
    expect(set_new_key_fn).not.toBeCalled()
  })

  it('Does not call new key if keys do not need updating', async () => {
    const { binary, log, files, set_new_key, set_new_key_fn } = setup('')

    await lastValueFrom(keygen(binary, log, { set_new_key })(files() as any))

    expect(set_new_key_fn).not.toBeCalled()
  })

  it('Calls new key if keypair is missing', async () => {
    const { binary, log, files, files_data, set_new_key, set_new_key_fn } = setup('')

    files_data.get.mockImplementationOnce(() => throwError(() => Error('')))

    await lastValueFrom(keygen(binary, log, { set_new_key })(files() as any))

    expect(set_new_key_fn).toBeCalledWith(expect.anything())
  })
})